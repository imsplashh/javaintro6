package primitives;

public class FloatingNumbers {
    public static void main(String[] args) {

        System.out.println("\n----------floating numbers----------");

       float myFloat1  = 20.5F;
       double myDouble1 = 20.5;

        System.out.println(myFloat1);
        System.out.println(myDouble1);

        System.out.println("\n----------floating numbers more----------");

       float myFloat2  = 10;
       double myDouble2 = 23435;

        System.out.println(myFloat2);
        System.out.println(myDouble2);

        System.out.println("\n----------floating numbers precision----------");


        float f1 = 32746237.23423542352352F;
        double d1 = 32746237.23423542352352;

        System.out.println(f1);
        System.out.println(d1);




    }
}
