package primitives;

public class Characters {
    public static void main(String[] args) {

        char c1 = 'A';
        char c2 = ' ';
        char myFavCharacter = '$';

        System.out.println(c1);
        System.out.println(c2);
        System.out.println(myFavCharacter);

        System.out.println("My favorite char = " + myFavCharacter);

    }
}
