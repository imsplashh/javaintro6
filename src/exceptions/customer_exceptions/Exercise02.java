package exceptions.customer_exceptions;

import utilities.ScannerHelper;

public class Exercise02 {
    public static void main(String[] args) {

        int day = ScannerHelper.getNumber();



        try{
            if(Exercise01.isCheckInHours(day)) System.out.println("This is check in hours");
        } catch (Exception e){
            System.out.println("This is not check in hours, try again!!!");
        }

    }
}
