package singleton;

public class Phone {
    
    public static Phone phone;

    private Phone(){
        // default constructor
    }

    public static Phone getPhone() {
        if(phone == null) phone = new Phone();
        return phone;
    }
}
