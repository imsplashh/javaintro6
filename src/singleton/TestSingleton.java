package singleton;

public class TestSingleton {
    public static void main(String[] args) {

        Phone p1 = Phone.getPhone();
        Phone p2 = Phone.getPhone();
        Phone p3 = Phone.getPhone();

        System.out.println(p1);
        System.out.println(p2);
        System.out.println(p3);

        Driver d1 = Driver.getDriver();
        Driver d2 = Driver.getDriver();
        Driver d3 = Driver.getDriver();

        System.out.println(d1);
        System.out.println(d2);
        System.out.println(d3);
    }
}
