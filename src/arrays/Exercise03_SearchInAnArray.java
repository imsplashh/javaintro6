package arrays;

import java.util.Arrays;

public class Exercise03_SearchInAnArray {
    public static void main(String[] args) {
        String[] objects = {"Remote", "Mouse", "Mouse", "Keyboard", "ipad"};

        /*
        Check the collection you have above and print true if it contains Mouse
        Print false otherwise

        RESULT:
        true
        */

        boolean hasMouse = false;

        for (String object : objects) {
            if(object.equals("Mouse")){
                hasMouse = true;
                break;
            }
        }
        System.out.println(hasMouse);

        // binary search

        Arrays.sort(objects);

        System.out.println(Arrays.binarySearch(objects, "Mouse") >= 0);
        System.out.println(Arrays.binarySearch(objects, "mouse") >= 0);
        System.out.println(Arrays.binarySearch(objects, "Keyboard") >= 0);
        System.out.println(Arrays.binarySearch(objects, "board") >= 0);






    }
}
