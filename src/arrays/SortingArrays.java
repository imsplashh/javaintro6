package arrays;

import java.util.Arrays;

public class SortingArrays {
    public static void main(String[] args) {

        int[] numbers = {5, 3, 10};
        String[] names = {"Alex", "ali", "John", "James"};

        // sort them

        Arrays.sort(numbers);
        Arrays.sort(names);


        // print arrays

        System.out.println(Arrays.toString(numbers));
        System.out.println(Arrays.toString(names));




    }
}
