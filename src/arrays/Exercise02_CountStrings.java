package arrays;

import java.util.Arrays;

public class Exercise02_CountStrings {
    public static void main(String[] args) {
        // Declare a string array called countries and assign a size of 3

        String[] countries = new String[3];

        countries[1] = "Spain";

        // print value index 1 and 2
        System.out.println(countries[1]);
        System.out.println(countries[2]);

        // Assign "Belgium" to index 0
        countries[0] = "Belgium";
        countries[2] = "Italy";

        // print array
        System.out.println(Arrays.toString(countries));

        // sort and print array
        Arrays.sort(countries);

        System.out.println(Arrays.toString(countries));

        // print each element with for i loop

        for (int i = 0; i < countries.length; i++) {
            System.out.println(countries[i]);
        }


        // print each element with for each loop

        for (String country : countries) {
            System.out.println(country);

        }

        //

        int count = 0;

        for (String country : countries) {
            if(country.length() == 5) count++;

        }
        System.out.println(count);

        // how many countries has letter I or i

        int countriesWithI = 0;

        for (String country : countries) {
            if(country.contains("i") || country.contains("I")) countriesWithI++;
        }
        System.out.println(countriesWithI);



    }
}
