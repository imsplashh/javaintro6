package arrays;

import java.util.Arrays;

public class UnderstandingArrays {
    public static void main(String[] args) {

        String[] cities = {"Chicago", "Miami", "Toronto", "Malibu"};

        //The number elements in array
        System.out.println(cities.length);


        // Get particular array
        System.out.println(cities[1]);
        System.out.println(cities[0]);
        System.out.println(cities[2]);

        //Print all arrays
        System.out.println(Arrays.toString(cities));
        
        
        //FORI LOOP
        //How to loop an array
        for (int i = 0; i < cities.length ; i++) {
            System.out.println(cities[i]);
        }

        //For each loop
        for (String city : cities) {
            System.out.println(city);

        }

    }
}
