package arrays;

import java.util.Arrays;

public class SearchingInAnArray {
    public static void main(String[] args) {

        int[] numbers = {3, 10, 8, 5, 5};

        boolean has7 = false;

        for (int number : numbers) {
            if(number == 7){
                has7 = true;
                break;
            }
        }
        System.out.println(has7);


        // binary search

        Arrays.sort(numbers);

        System.out.println(Arrays.binarySearch(numbers, 10));
        System.out.println(Arrays.binarySearch(numbers, 5));

        System.out.println(Arrays.binarySearch(numbers, 1));




    }
}
