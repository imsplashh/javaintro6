package arrays;

import utilities.ScannerHelper;

public class Exercise04_CountChars {
    public static void main(String[] args) {

        System.out.println("\n------------1st way---------------");

        String str = ScannerHelper.getString();

        int countL = 0;

        for (int i = 0; i < str.length(); i++) {
            if(Character.isLetter(str.charAt(i))) countL++;
        }


        System.out.println(countL);


        System.out.println("\n------------2nd way---------------");

        str = ScannerHelper.getString();

        countL = 0;

        char[] chars = str.toCharArray();

        for (char c : chars) {
            if(Character.isLetter(c)) countL++;
        }

        System.out.println(countL);





    }
}
