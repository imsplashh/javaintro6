package arrays;

import java.util.Arrays;

public class DoubleArray {
    public static void main(String[] args) {

        //create an array
        System.out.println("\n----------TASK-1----------");
        double[] numbers = {5.5, 6, 10.3, 25};


        //print the array
        System.out.println("\n----------TASK-2----------");

        System.out.println(Arrays.toString(numbers));

        //print the length of the array
        System.out.println("\n----------TASK-3----------");

        System.out.println("The length is " + numbers.length);


        //print each element using for each loop
        System.out.println("\n----------TASK-4----------");

        for (double number : numbers) {
            System.out.println(number);
        }

    }
}
