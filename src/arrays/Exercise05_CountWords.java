package arrays;

import utilities.ScannerHelper;

import java.util.Arrays;

public class Exercise05_CountWords {
    public static void main(String[] args) {

        /*

         */

        String str = ScannerHelper.getString();

        System.out.println(str.split(" ").length);

        System.out.println("\n------how to avoid miscounting spaces-------");

        String str3 = "Hello   World"; // 2 words

        String[] arr = str3.split(" ");

        System.out.println(Arrays.toString(arr)); // [Hello, , , World]

        int actualWords = 0;

        for (String s : arr) {
            if(!s.isEmpty()) actualWords++;
        }

        System.out.println(actualWords); // 2

    }
}
