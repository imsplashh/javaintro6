package arrays;

import java.util.Arrays;

public class TwoDimensionalArrays {
    public static void main(String[] args) {

        String[][] students = {
                {"Meerim", "Alina", "Carmela", "Ayat"},
                {"Yahya", "Adam", "Louie"},
                {"Dima", "Lesia", "Pinar"}
        };

        //How to print 2-dimensional array
        System.out.println(Arrays.deepToString(students));

        // How to print inner array
        System.out.println(Arrays.toString(students[0]));
        System.out.println(Arrays.toString(students[1]));
        System.out.println(Arrays.toString(students[2]));

        // How to print an element in a inner array
        System.out.println(students[1][2]);

        //How to print the length of the 2-dimensional array
        System.out.println(students.length);

        //How to print the length of each inner array
        System.out.println(students[0].length);

        System.out.println("\n-------Looping a 2-dimensional array-----\n");
        // How to loop a 2-dimensional array
        for (int i = 0; i < students.length; i++) {
            System.out.println(Arrays.toString(students[i]));
        }

        for(String[] innerArray : students){
            System.out.println(Arrays.toString(innerArray));
        }


        System.out.println("\n-------Looping each element with for each loop-----\n");
        // How to loop a 2-dimensional array for each element
        for(String[] inner : students){
            //Each inner array is here
            for(String name : inner){
                //Each name is here
                System.out.println(name);
            }
        }

        System.out.println("\n-------Looping each element with fori loop-----\n");
        for (int i = 0; i < students.length; i++) {
            //Each inner array is here
            for (int j = 0; j <  students[i].length; j++) {
                System.out.println(students[i][j]);
            }
        }

        //create a container that will store 5 groups of 3 numbers
        int[][] numbers = new int[5][3];
        System.out.println(Arrays.deepToString(numbers));

        numbers[3][1] = 9;
        numbers[2][0] = 5;
        numbers[0][0] = 7;




    }
}
