package arrays;

import com.sun.xml.internal.ws.api.model.wsdl.WSDLOutput;

import java.util.Arrays;

public class IntArray {
    public static void main(String[] args) {
        int[] numbers = new int[6];

        System.out.println(Arrays.toString(numbers));

        //How to assign a value to existing array/ index

        numbers[0] = 5;
        numbers[2] = 15;
        numbers[4] = 25;

        System.out.println(Arrays.toString(numbers));
        numbers[0] = 7;

        System.out.println(Arrays.toString(numbers));


        System.out.println("\n----------TASK-1----------");
        //for i loop

        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);

        }

        System.out.println("\n----------TASK-2----------");
        //for each loop

        for (int num : numbers) {
            System.out.println(num);
        }



    }

}
