package arrays;

public class Exercise01_CountNumbers {
    public static void main(String[] args) {

        System.out.println("\n----------TASK-1----------");
        int[] numbers = {-1, 3, 0, 5, -7, 10, 8, 0, 10, 0};

        int negatives = 0;

        for (int number : numbers) {
           if(number < 0)
               negatives++;
        }
        System.out.println(negatives);


        System.out.println("\n----------TASK-2----------");
        // even numbers

        int evenNumbers = 0;

        for (int number : numbers) {
            if(number % 2 == 0) evenNumbers++;
        }

        System.out.println(evenNumbers);


        System.out.println("\n----------TASK-3----------");
        //find sum of array

        int sum = 0;

        for (int number : numbers) {
            sum += number;
        }
        System.out.println(sum);







    }
}
