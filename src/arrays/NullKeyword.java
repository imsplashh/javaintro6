package arrays;

public class NullKeyword {
    public static void main(String[] args) {

        // null means nothing

        String s1 = null;
        String s2 = "Jehad";

        System.out.println(s1); // null
        System.out.println(s2); // Jehad

        //System.out.println(s1.length()); // NullPointerException
        System.out.println(s1.charAt(1)); // NullPointerException
        System.out.println(s2.length()); // 5




    }
}
