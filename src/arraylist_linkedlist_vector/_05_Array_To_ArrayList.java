package arraylist_linkedlist_vector;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class _05_Array_To_ArrayList {
    public static void main(String[] args) {

        /*
        HOW TO CONVERT AN ARRAY TO ARRAYLIST
         */

        System.out.println("------- Method 1 asList--------");

        ArrayList<String> cities = new ArrayList<>(Arrays.asList("Berlin", "Paris", "Rome"));

        System.out.println(cities);


        System.out.println("------- Method 2 --------");
        String[] countries = {"USA", "Germany", "Spain", "Italy"};

        ArrayList<String> list = new ArrayList<>();

        for (String country : countries) {
            list.add(country);
        }

        System.out.println(list);



        System.out.println("------- Method 3 addAll. --------");

        list = new ArrayList<>();

        Collections.addAll(list, countries);

        System.out.println(list);


    }
}
