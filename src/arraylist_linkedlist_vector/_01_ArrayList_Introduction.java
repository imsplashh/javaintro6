package arraylist_linkedlist_vector;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class _01_ArrayList_Introduction {
    public static void main(String[] args) {

        //1. How to create an array vs ArrayList
        System.out.println("\n------TASK-1------");

        String[] array = new String[3];

        ArrayList<String> list = new ArrayList<>(); // IF NO CAPACITY DECLARED, CAPACITY = 10 BY DEFAULT


        //2. How to get the size of the Array and ArrayList
        System.out.println("\n------TASK-2------");
        System.out.println("The size of the array = " + array.length);
        System.out.println("The size of the list = " + list.size());


        //3. How to print Array vs ArrayList
        System.out.println("\n------TASK-3------");
        System.out.println("The array = " + Arrays.toString(array));
        System.out.println("The list = " + list);


        //4. How to add elements to an array vs arrayList
        System.out.println("\n------TASK-4------");
        array[1] = "Alex";
        array[2] = "Max";
        array[0] = "John";
        System.out.println("The array = " + Arrays.toString(array));

        list.add("Joe");  // in chronological order
        list.add("Jane");
        list.add("Mike");
        list.add("Adam");
        list.add(2, "Jazzy"); // assigns the name to an index
        list.add(5, "Yahya"); // indexoutofbounds
        System.out.println("The list = " + list);



        //5. How to update an existing element in an array vs arrayList
        System.out.println("\n------TASK-5------");

        array[1] = "Ali";
        System.out.println("The array = " + Arrays.toString(array));

        list.set(1, "Jasmine"); // set method replaces an element and reassigns it
        System.out.println("The list = " + list);



        //6. How to retrieve/ get element from array vs arrayList
        System.out.println("\n------TASK-6------");
        System.out.println(array[2]);

        System.out.println(list.get(3));


        //7. How to loop an array vs arrayList
        System.out.println("\n------TASK-7 for i loop------");
        for (int i = 0; i < array.length; i++) {
            System.out.println(array[i]);
        }

        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }




        System.out.println("\n------TASK-7 for each loop------");
        for (String element : array) {
            System.out.println(element);
        }

        for (String element : list) {
            System.out.println(element);
        }


        System.out.println("\n------TASK-7 for each loop method behind the scenes------");

        list.forEach(System.out::println); // landa expression





    }
}
