package arraylist_linkedlist_vector;

import java.util.ArrayList;
import java.util.Arrays;

public class _03_Group_Methods {
    public static void main(String[] args) {

        System.out.println("\n----- Task 1 -----");


        ArrayList<String> group1Members = new ArrayList<>();
        group1Members.add("Belal");
        group1Members.add("Assem");
        group1Members.add("Gurkan");
        group1Members.add("Dima");


        ArrayList<String> group2Members = new ArrayList<>();
        group2Members.add("Adam");
        group2Members.add("Melek");
        group2Members.add("Cihan");


        ArrayList<String> group3Members = new ArrayList<>();
        group3Members.add("Yousef");
        group3Members.add("Sandina");

        System.out.println("Group 1 members = " + group1Members);
        System.out.println("Group 2 members = " + group2Members);
        System.out.println("Group 3 members = " + group3Members);

        /*

         */

        ArrayList<String> allMembers = new ArrayList<>();
        allMembers.addAll(group2Members);
        allMembers.addAll(group3Members);
        allMembers.addAll(3, group1Members); // Assigns the members to that index

        System.out.println("All members = " + allMembers);



        System.out.println("\n----- Task 2 -----");

        ArrayList<String> elementsToRemove = new ArrayList<>();
        elementsToRemove.add("Adam");
        elementsToRemove.add("Assem");
        elementsToRemove.add("Yousef");

        allMembers.removeAll(elementsToRemove);
        System.out.println(allMembers);

        allMembers.removeIf(element -> element.equals("Adam") || element.equals("Assem") || element.equals("Yousef"));

        System.out.println(allMembers);


        ArrayList<String> check1 = new ArrayList<>(Arrays.asList("Cihan", "Dima", "Sandina"));
        ArrayList<String> check2 = new ArrayList<>(Arrays.asList("Jazzy", "Gurkan", "Belal", "Melek"));

        System.out.println(check1);
        System.out.println(check2);

        System.out.println(allMembers.containsAll(check1));
        System.out.println(allMembers.containsAll(check2));

        /*
        Keep only Cihan and Gurkan

         */
        System.out.println("\n----- Task 3 -----");


        allMembers.removeIf(element -> !element.equals("Cihan") && !element.equals("Gurkan"));

        System.out.println(allMembers);


    }
}
