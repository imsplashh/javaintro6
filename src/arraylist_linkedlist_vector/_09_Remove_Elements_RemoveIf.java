package arraylist_linkedlist_vector;

import java.util.ArrayList;
import java.util.Arrays;

public class _09_Remove_Elements_RemoveIf {
    public static void main(String[] args) {

       //Remove all colors with "R" or "r"
        ArrayList<String> colors = new ArrayList<>(Arrays.asList("Red", "Purple", "Blue", "Yellow"));

        System.out.println(colors);

        colors.removeIf(element -> element.toLowerCase().contains("r"));

        System.out.println(colors);



    }
}
