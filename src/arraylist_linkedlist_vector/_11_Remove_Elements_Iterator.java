package arraylist_linkedlist_vector;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

public class _11_Remove_Elements_Iterator {
    public static void main(String[] args) {

        ArrayList<Integer> numbers = new ArrayList<>(Arrays.asList(10, 15, 12, 5, 3, 0, 30));

        /*
        ArrayList<Integer> newList = new ArrayList<>();

        for (Integer number : numbers) {
            if((number <= 10)) newList.add(number);
        }

        System.out.println(newList);

         */

        Iterator<Integer> iterator = numbers.iterator();

       while(iterator.hasNext()){
           if(iterator.next() > 10) iterator.remove();
       }
        System.out.println(numbers);
    }
}
