package arraylist_linkedlist_vector;

import java.util.ArrayList;

public class _02_Additional_Methods {
    public static void main(String[] args) {

        /*
        Create an ArrayList to store below numbers
        10
        15
        20
        10
        20
        30
        Print the ArrayList
        Print the size

        EXPECTED:
        [10, 15, 20, 10, 20, 30]
        6
         */

        System.out.println("\n----- Task 1 -----");
        ArrayList<Integer> numbers = new ArrayList<>();
        numbers.add(10);
        numbers.add(15);
        numbers.add(20);
        numbers.add(10);
        numbers.add(20);
        numbers.add(30);

        System.out.println(numbers);
        System.out.println(numbers.size());


        System.out.println("\n----- Task 2 -----");

        System.out.println(numbers.contains(5));
        System.out.println(numbers.contains(10));
        System.out.println(numbers.contains(20));


        System.out.println("\n----- Task 3 -----");

        /*
        What is the first occurrence index of the element 10
        What is the first occurrence index of the element 20
        What is the last occurrence index of the element 10
        What is the last occurrence index of the element 20

        EXPECTED OUTPUT:
        0
        2
        3
        4
         */

        System.out.println(numbers.indexOf(10));
        System.out.println(numbers.indexOf(20));
        System.out.println(numbers.lastIndexOf(10));
        System.out.println(numbers.lastIndexOf(20));


        System.out.println("\n----- Task 4 -----");


        numbers.remove((Integer) 15);
        numbers.remove((Integer) 30);
        numbers.remove((Integer) 100); // does not remove it and returns false

        System.out.println(numbers);

        numbers.remove((Integer) 10); //if multiples of that number, removes first one

        System.out.println(numbers);

        // removes all the numbers you say
        numbers.removeIf(element -> element == 20); //lambda expression
        System.out.println(numbers);


        System.out.println("\n----- Task 5 -----");
        numbers.add(11);
        numbers.add(12);

        System.out.println(numbers);
        numbers.clear();

        System.out.println(numbers);






    }
}
