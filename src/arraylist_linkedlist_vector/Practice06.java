package arraylist_linkedlist_vector;

import java.lang.reflect.Array;
import java.util.*;

public class Practice06 {
    public static void main(String[] args) {

        System.out.println("------ TASK 1 -------");

        /*
        Write a method called as
        double
         to double each element
        in an int array and return it back.
        NOTE: The return type is an array.
        Test data 1:
        {3, 2, 5, 7, 0}
        Expected output 1:
        [6, 4, 10, 14, 0]
        Test data 2:
        [-2, 0, 3, 10, 100]
        Expected output 2:
        [-4, 0, 6, 20, 200]
        NOTE: Make your code dynamic that works for any given
        array.
         */


        System.out.println(Arrays.toString(double1(new int[] {3, 2, 5, 7, 0})));


        System.out.println("------ TASK 2 -------");

        /*
        Requirement:
        Write a method called as
        secondMax
         to find and return
        the second max number in an ArrayList
        Test data 1:
        {2, 3, 7, 1, 1, 7, 1}
        Expected output 1:
        3
        Test data 2:
        [5, 7, 2, 2, 10, 10]
        Expected output 2:
        7
        NOTE: Make your code dynamic that works for any
        given ArrayList
         */

        ArrayList<Integer> list = new ArrayList<>(Arrays.asList(2, 3, 7, 1, 1, 7, 1));
        System.out.println(secondMax(list));

        System.out.println("------ TASK 3 -------");

        /*
        Requirement:
        Write a method called as
        secondMin
         to find and return
        the second min number in an ArrayList
        Test data 1:
        {2, 3, 7, 1, 1, 7, 1}
        Expected output 1:
        2
        Test data 2:
        [5, 7, 2, 2, 10, 10]
        Expected output 2:
        5
        NOTE: Make your code dynamic that works for any
        given ArrayList


         */
        ArrayList<Integer> list2 = new ArrayList<>(Arrays.asList(2, 3, 7, 1, 1, 7, 1));
        System.out.println(secondMin(list2));


        System.out.println("------ TASK 4 -------");

        ArrayList<String> list3 = new ArrayList<>(Arrays.asList("Tech", "Global", "", null, "", "School"));
        System.out.println(removeEmpty(list3));



        System.out.println("------ TASK 5 -------");

        ArrayList<Integer> list4 = new ArrayList<>(Arrays.asList(-12, -123, -5, 1000, 500, 0));
        System.out.println(remove3orMore(list4));


        System.out.println("------ TASK 6 -------");

        /*
        Requirement:
        Write a method called as
        uniquesWords
        to find and return all the
        unique words in a String.
        NOTE: The return type is an ArrayList.
        NOTE: Assume that you will not be given extra spaces.
        Test data 1:
        "TechGlobal School”
        Expected output 1:
        ["TechGlobal", "School"]
        Test data 2:
        "Star Light Star Bright"
        Expected output 2:
        ["Star", "Light", "Bright"]
        NOTE: Make your code dynamic that works for any given String
         */


        System.out.println(uniqueWords("Star Light Star Bright"));








    }

    public static int[] double1(int[] arr){

        for (int i = 0; i < arr.length; i++) {
            arr[i] *= 2;
        }
        return arr;

    }
    public static int secondMax(ArrayList<Integer> list){

        int max = Integer.MIN_VALUE;
        int secondMax = Integer.MIN_VALUE;

        for (Integer n : list) {
            if(n > max){
                secondMax = max;
                max = n;
            }
        }
        return secondMax;


    }
    public static int secondMin(ArrayList<Integer> list2) {
        int min = Integer.MAX_VALUE;
        int secondMin = Integer.MAX_VALUE;

        Collections.sort(list2);
        for (int i = 1; i < list2.size(); i++) {
            if(list2.get(i) > list2.get(0)) return list2.get(i);
/*
        for (Integer n : list2) {
            if (n < min) {
                secondMin = min;
                min = n;


            }
        }
        return secondMin;


*/

        }
        return 0;
    }

    public static ArrayList<String> removeEmpty(ArrayList<String> list3){


        //WAY 1
        list3.removeIf(element -> element == null || element.isEmpty());
        return list3;


       /* WAY2
       ArrayList<String> noEmpty = new ArrayList<>();
       for (String s : list3) {
            if(!s.isEmpty()) noEmpty.add(s);
        }
        return noEmpty;


        */

    }
    public static ArrayList<Integer> remove3orMore(ArrayList<Integer> list4){
        list4.removeIf(e -> e >= 100 || e <= -100);
        return list4;
    }

    public static ArrayList<String> uniqueWords(String str){

        ArrayList<String> strAsList = new ArrayList<>();
        for (String s : str.split(" ")) {
            if(!strAsList.contains(s)) strAsList.add(s);

        }
        return strAsList;

    }
}
