package practice05;

public class Exercise06 {
    public static void main(String[] args) {

        /*
        Write a program to print duplicated characters in a
        String, ignore cases

        Test data 1:
        String str = ”baNana”;

        Expected output 1:
        a
        N

        Test data 2:

        String str = ”aPple”;
        Expected output 1:
        P
         */
        duplicateLetters("baNana");




    }
    public static void duplicateLetters(String str){
        String answer = "";


        for (int i = 0; i < str.length()-1; i++) {
            for (int j = i + 1; j < str.length(); j++) {
                if (str.toLowerCase().charAt(i) == str.toLowerCase().charAt(j) && !answer.contains("" + str.charAt(i)))
                    answer += str.charAt(i);
                }
            }
            char[] answerArr = answer.toCharArray();
            for (char c : answerArr) {
                System.out.println(c);
            }
      }
}
