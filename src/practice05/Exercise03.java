package practice05;

import utilities.RandomGenerator;
import utilities.ScannerHelper;

import java.util.Arrays;

public class Exercise03 {
    public static void main(String[] args) {

        /*
        Write a program to generate 5 random numbers
        between 1 to 10 (1 and 10 are included) and store those
        numbers in an int array.
        Find the max and min numbers among the random
        numbers and print them.
        Solve this question with sort and without sort.
         */

        minAndMaxUsingSort();
        minAndMaxWithoutSort();




    }
    public static void minAndMaxUsingSort(){
        int[] randomArr = {RandomGenerator.getRandomNumber(1,10),
                RandomGenerator.getRandomNumber(1,10),
                RandomGenerator.getRandomNumber(1,10),
                RandomGenerator.getRandomNumber(1,10),
                RandomGenerator.getRandomNumber(1,10)};


        System.out.println(Arrays.toString(randomArr));

        Arrays.sort(randomArr);
        System.out.println(Arrays.toString(randomArr));

        System.out.println("The max number is = " + randomArr[4]);
        System.out.println("The min number is = " + randomArr[0]);





        }





    public static void minAndMaxWithoutSort(){
        int[] randomArr = {RandomGenerator.getRandomNumber(1,10),
                           RandomGenerator.getRandomNumber(1,10),
                           RandomGenerator.getRandomNumber(1,10),
                           RandomGenerator.getRandomNumber(1,10),
                           RandomGenerator.getRandomNumber(1,10)};


        System.out.println(Arrays.toString(randomArr));

        int min = 10;
        int max = 1;

        for (int i : randomArr) {
            if(i > max) max = i;
            else if(i < min) min = i;
        }
        System.out.println("The max number is = " + max);
        System.out.println("The min number is = " + min);

    }




}
