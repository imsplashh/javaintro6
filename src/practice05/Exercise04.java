package practice05;

public class Exercise04 {
    public static void main(String[] args) {

        /*
        Write a program to find if String array contains “apple”
        as an element, ignore cases.
        Test data 1:
        String[] list = {“banana”, “orange”, “Apple”};
        Expected output:
        true
        Test data 2:
        String[] list = {“pineapple”, “banana”, “orange”,
        “grapes”};
        Expected output:
        false
         */

        containsApple();




    }

    public static void containsApple(){
        String[] list = {"banana", "orange", "Apple"};
        boolean hasApple = false;

        for (String s : list) {
            if(s.equalsIgnoreCase("apple")) hasApple = true;
        }
        System.out.println(hasApple);

        String[] list2 = {"pineapple", "banana", "orange",
"grapes"};
        boolean hasApple2 = false;

        for (String s : list2) {
            if(s.equalsIgnoreCase("apple")) hasApple2 = true;
        }
        System.out.println(hasApple2);



    }

}
