package projects;

import sun.font.FontRunIterator;

import java.util.Arrays;

public class Project08 {
    public static void main(String[] args) {

        System.out.println("===== task 1 ======");

        System.out.println(findClosestDistance(new int[] {4, 8, 7, 15}));


        System.out.println("===== task 2 ======");

        System.out.println(findSingleNumber(new int[] {5, 3, -1, 3, 5, 7, -1}));


        System.out.println("===== task 3 ======");

        System.out.println(findFirstUniqueCharacter(new String("abc abc d")));


        System.out.println("===== task 4 ======");

        System.out.println(findMissingNumber(new int[]{2, 4}));



    }

    public static int findClosestDistance(int[] arr){
       // System.out.println("===== task 1 ======");

        /*
        TASK-1 - findClosestDistance() method
•	Write a method that takes an int[] array as an argument and returns the closest difference between the numbers.
•	This method will return an int which is the closest difference between 2 elements in the array
•	NOTE: if array does not have at least 2 elements, then return -1.

         */

     int diff = Integer.MAX_VALUE;

        for (int i = 0; i < arr.length; i++)
            for (int j = i + 1; j < arr.length; j++) {
                if(Math.abs(arr[i] - arr[j]) < diff)
                diff =  Math.abs(arr[i] - arr[j]);
            }
        return diff;

    }
    public static int findSingleNumber(int[] arr){
       // System.out.println("===== task 2 ======");

        /*
        TASK-2 – findSingleNumber() method
•	Write a method that takes an int[] array as an argument and returns the element occurs only once.
•	You will be given a non-empty array in which all the elements appear at least twice except for one.

         */
            int result = 0;
            for (int num : arr) {
                result ^= num;
            }

        return result;
    }

    public static char findFirstUniqueCharacter(String str) {
        // System.out.println("===== task 3 ======");


        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            if (str.indexOf(c) == str.lastIndexOf(c)){
                return c;
            }
        }
        return 0;
    }

    public static int findMissingNumber(int[] arr){
        // System.out.println("===== task 4 ======");

        Arrays.sort(arr);

        for (int i = 0; i < arr.length; i++) {
            if (arr[i + 1] - arr[i] != 1) {
                return arr[i] + 1;
            }
        } return 0;



    }
}
