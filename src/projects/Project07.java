package projects;

import java.util.ArrayList;
import java.util.Arrays;

public class Project07 {
    public static void main(String[] args) {

       // System.out.println("====== TASK 1 ======");

        System.out.println(countMultipleWords(new String[]{"foo”, “”, “ “, “foo bar”, “java is fun”, “ ruby "}));


       // System.out.println("====== TASK 2 ======");

        ArrayList<Integer> list = new ArrayList<>(Arrays.asList(2, -5, 6, 7, -10, -78, 0, 15));
        System.out.println(removeNegatives(list));


       // System.out.println("====== TASK 3 ======");


        System.out.println(validatePassword(new String("Abcd123!")));

       // System.out.println("====== TASK 4 ======");

        System.out.println(validateEmailAddress(new String("abcd@gmail.com")));


    }

    public static int countMultipleWords(String[] arr) {

        /*
        TASK-1 - countMultipleWords() method
        •	Write a method that takes a String[] array as an argument and counts how many strings in the array has multiple words.
        •	This method will return an int which is the count of elements that have multiple words.
        •	NOTE: be careful about these as they are not multiple words ->“”,    “   “,    “    abc”,  “abc   “

        Test data:
        [“foo”, “”, “ “, “foo bar”, “java is fun”, “ ruby ”]

         */
        System.out.println("====== TASK 1 ======");

        int count = 0;

        for (String s : arr) {

            if (!(s.isEmpty())) {
                if (s.trim().contains(" "))
                    count++;
            }
        }

        return count;

    }


    public static ArrayList<Integer> removeNegatives(ArrayList<Integer> list) {

        System.out.println("====== TASK 2 ======");

        /*
            TASK-2 - removeNegatives() method
        •	Write a method that takes an “ArrayList<Integer> numbers” as an argument and removes all negative numbers from the given list if there are any.
        •	This method will return an ArrayList with removed negatives.

        Test data 1:
        [2, -5, 6, 7, -10, -78, 0, 15]

         */


        list.removeIf(e -> e < 0);

        return list;

    }

    public static boolean validatePassword(String password) {

        System.out.println("====== TASK 3 ======");

        /*
        TASK-3 - validatePassword() method
        •	Write a method that takes a “String password” as an argument and checks if the given password is valid or not.
        •	This method will return true if given password is valid, or false if given password is not valid.
        •	A VALID PASSWORD:
            -should have length of 8 to 16 (both inclusive).
            -should have at least 1 digit, 1 uppercase, 1 lowercase and 1 special char.
            -should NOT have any space.

        Test data 1:

        Expected output 1:
        false

         */

        if (password.length() < 8 || password.length() > 16 || password.contains(" ")) {
            return false;
        }
        boolean hasUppercase = false;
        boolean hasLowercase = false;
        boolean hasDigit = false;
        for (int i = 0; i < password.length(); i++) {
            char c = password.charAt(i);
            if (Character.isUpperCase(c)) {
                hasUppercase = true;
            } else if (Character.isLowerCase(c)) {
                hasLowercase = true;
            } else if (Character.isDigit(c)) {
                hasDigit = true;
            }
        }
        return hasUppercase && hasLowercase && hasDigit;
    }

    public static boolean validateEmailAddress(String email){

        System.out.println("====== TASK 4 ======");

        /*
        TASK-4 - validateEmailAddress() method
        •	Write a method that takes a “String email” as an argument and checks if the given email is valid or not.
        •	This method will return true if given email is true, or false if given email is not valid.
        •	A VALID EMAIL:
            -should NOT have any space.
            -should not have more than one “@” character.
            -should be in the given format <2+chars>@<2+chars>.<2+chars>

        Test data 1:
        a@gmail.com

        Expected output 1:
        false

         */


        if(email.contains(" ") || email.contains("@@") || email.contains("..") || email.contains(".@")
        || email.contains("@.") || email.contains("._.")){
            return false;
        }

        if(email.indexOf("@") < 2 || email.indexOf("@") != email.lastIndexOf("@")){
            return false;
        }
        int dot = email.lastIndexOf(".");
        if(dot < email.indexOf("@") + 2 || dot >= email.length() + 2 || dot <= email.indexOf("@")){
            return false;
        }
        return true;


    }

}