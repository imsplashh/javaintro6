package projects;

import static javafx.scene.input.KeyCode.J;

public class Project01 {
    public static void main(String[] args) {

        System.out.println("\n----------TASK-1----------");

        String name = "Jehad";

        System.out.println("My name is " + name);



        System.out.println("\n----------TASK-2----------");

        char nameCharacter1;
        char nameCharacter2;
        char nameCharacter3;
        char nameCharacter4;
        char nameCharacter5;

        nameCharacter1 = 'J';
        nameCharacter2 = 'e';
        nameCharacter3 = 'h';
        nameCharacter4 = 'a';
        nameCharacter5 = 'd';

        System.out.println("Name letter 1 is " + nameCharacter1);
        System.out.println("Name letter 2 is " + nameCharacter2);
        System.out.println("Name letter 3 is " + nameCharacter3);
        System.out.println("Name letter 4 is " + nameCharacter4);
        System.out.println("Name letter 5 is " + nameCharacter5);



        System.out.println("\n----------TASK-3----------");

        String myFavMovie = " The Expandable's";
        String myFavSong = " Dammi Falastini ";
        String myFavCity = " Chicago ";
        String myFavActivity = " Playing Basketball ";
        String myFavSnack = " Hot Cheeto popcorn";

        System.out.println("My Favorite movie is " + myFavMovie);
        System.out.println("My Favorite song is " + myFavSong);
        System.out.println("My Favorite city is " + myFavCity);
        System.out.println("My Favorite activity is " + myFavActivity);
        System.out.println("My Favorite snack is " + myFavSnack);




        System.out.println("\n----------TASK-4----------");

        int myFavNumber = 7;
        int numberOfStatesIVisited = 8;
        int numberOfCountriesIVisited = 2;

        System.out.println("My favorite number is " + myFavNumber);
        System.out.println("The total number of states I visited is " + numberOfStatesIVisited);
        System.out.println("The total number of countries I visited is  " + numberOfCountriesIVisited);




        System.out.println("\n----------TASK-5----------");

        boolean amIAtSchooToday =  false;

        System.out.println("I am at school today = " + amIAtSchooToday);



        System.out.println("\n----------TASK-6----------");

        boolean isWeatherNiceToday = false;

        System.out.println("The weather was nice today = " + isWeatherNiceToday);



    }

}
