package projects;

public class Project03 {
    public static void main(String[] args) {

        System.out.println("\n----------TASK-1----------");

        String s1 = "24", s2 = "5";

        int S1 = Integer.parseInt(s1);
        int S2 = Integer.parseInt(s2);

        double A1 = Integer.parseInt(s1);
        double A2 = Integer.parseInt(s2);

        System.out.println("The sum of 24 and 5 = " + (S1 + S2));
        System.out.println("The subtraction of 24 and 5 = " + (S1 - S2));
        System.out.println("The division of 24 and 5 = " + (A1 / A2));
        System.out.println("The multiplication of 24 and 5 = " + (S1 * S2));
        System.out.println("The remainder of 24 and 5 = " + (S1 % S2));



        System.out.println("\n----------TASK-2----------");

        int random = (int) (Math.random() * 35 + 1);
        System.out.println(random);

       if(random % 2 ==1){
           System.out.println(random + " IS A PRIME NUMBER");
       } else{
           System.out.println(random + " IS NOT A PRIME NUMBER");
       }


        System.out.println("\n----------TASK-3----------");

        int random1 = (int) (Math.random() *50 + 1);
        int random2 = (int) (Math.random() *50 + 1);
        int random3 = (int) (Math.random() *50 + 1);



        System.out.println(random1);
        System.out.println(random2);
        System.out.println(random3);

        int max = Math.max(random1, Math.max(random2, random3));
        int min = Math.min(random1, Math.min(random2, random3));

        if(random1 != max && random1 != min){
            System.out.println("Lowest number is  = " +min + "\nMiddle number is = " + random1 + "\nGreatest number is = " + max);

        }  else if (random2 != max && random2 !=min){
            System.out.println("Lowest number is  = " +min + "\nMiddle number is = " + random2 + "\nGreatest number is = " + max);
        } else if (random3 != max && random3 != min){
            System.out.println("Lowest number is  = " +min + "\nMiddle number is = " + random3 + "\nGreatest number is = " + max);
        }


        System.out.println("\n----------TASK-4----------");


        char c = 'R';


        if(c >= 65 && c <=90){
            System.out.println("The letter is uppercase");

        } else if (c >= 97 && c <= 122){
            System.out.println("The letter is lowercase");
        } else{
            System.out.println("Invalid character detected!!!");
        }


        System.out.println("\n----------TASK-5----------");


        char c1 = 'e';
        System.out.println(c1);


        if(c1 <= 65 || c1 >= 122){
            System.out.println("Invalid character detected!!!");

        } else if (c1 == 65|| c1 ==69 || c1 == 73 || c1 == 79 || c1 == 85 ||
                c1 == 97 || c1 == 101 || c1 == 105 || c1 == 111 || c1 == 117){
            System.out.println("The letter is vowel");

        } else {
            System.out.println("The letter is a consonant");
        }




        System.out.println("\n----------TASK-6----------");


        char c2 = '8';
        System.out.println(c2);

        if (c2 >=32 && c2 <=47 || c2 >= 58 && c2 <= 64 || c2 >= 91 && c2 <=96 ||
        c2 >=123 && c2 <= 126){
            System.out.println("Special character is = " +c2);
        } else {
            System.out.println("Invalid character detected!!!");
        }




        System.out.println("\n----------TASK-7----------");

        char c3 = '5';
        System.out.println(c3);

        if (c3 >= 65 && c3 <= 90 || c3 >= 97 && c3 <= 122){
            System.out.println("Character is a letter");

        } else if (c3 >= 48 && c3 <= 57) {
            System.out.println("Character is a digit");

        } else {
            System.out.println("Character is a special character");
        }


    }
}
