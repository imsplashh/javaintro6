package projects;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Project06 {
    public static void main(String[] args) {

        System.out.println("------TASK 1 ------");

       int[] numbers = {10, 7, 7, 10, -3, 10, -3};

        Arrays.sort(numbers);

        System.out.println("Smallest = " + numbers[0]);
        System.out.println("Greatest = " + numbers[6]);

        System.out.println("------TASK 2 ------");

        int[] numbers2 = {10, 7, 7, 10, -3, 10, -3};

        int max = Arrays.stream(numbers2).max().getAsInt();
        int min = Arrays.stream(numbers2).min().getAsInt();

        System.out.println("Smallest = " + min);
        System.out.println("Greatest = " + max);


        System.out.println("------TASK 3 ------");

        int[] numbers3 = {10, 5, 6, 7, 8, 5, 15, 15};

        Arrays.sort(numbers3);

        System.out.println("The second smallest " + numbers3[2]);
        System.out.println("The second greatest " + numbers3[5]);


        System.out.println("------TASK 4 ------");

        int[] numbers4 = {10, 5, 6, 7, 8, 5, 15, 15};

        int secondMax = Arrays.stream(numbers4).max().getAsInt()-5;
        int secondMin = Arrays.stream(numbers4).min().getAsInt()+1;

        System.out.println("Smallest = " + secondMin);
        System.out.println("Greatest = " + secondMax);


        System.out.println("------TASK 5 ------");

        String[] array = {"foo", "bar", "Foo", "bar", "6", "abc", "6", "xyz"};

        for (int i = 0; i < array.length-1; i++) {
            for (int j = i + 1; j < array.length; j++) {
                if(array[i].equals(array[j])){

                    System.out.println(array[j]);

                }
            }
        }

        System.out.println("------TASK 6 ------");

        String[] array2 = {"pen", "eraser", "pencil", "pen", "123", "abc", "pen", "eraser"};

        int length = array2.length;
        int maxCount = 0;
        String maxFreq = "0";

        for (int i = 0; i < length; i++) {
            int count = 0;
            for (int j = 0; j < length; j++) {
                if(array2[i] == array2[j]){
                    count++;
                }
            }
            if(count > maxCount){
                maxCount = count;
                maxFreq = array2[i];
            }
        }

        System.out.println("The most frequent element is: " + maxFreq);

    }
}
