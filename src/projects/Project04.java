package projects;

import com.sun.xml.internal.ws.api.model.wsdl.WSDLOutput;
import utilities.ScannerHelper;

public class Project04 {
    public static void main(String[] args) {

        System.out.println("\n----------TASK-1----------");


        String str = ScannerHelper.getString();

        if (str.length() >= 8) {
            System.out.println(str.substring(str.length() - 4) + str.substring(4, str.length() - 4) + str.substring(0, 4));
        } else System.out.println("This String does not have 8 characters");



        System.out.println("\n----------TASK-2----------");


        String sentence = ScannerHelper.getString();

        String last = sentence.substring(sentence.lastIndexOf(" ") + 1);
        String middle = sentence.substring(sentence.indexOf(" "), sentence.lastIndexOf(" "));
        String first = sentence.substring(0, sentence.indexOf(" "));


        if (sentence.contains(" ")) {
            System.out.println(last +  " " + first);

        } else System.out.println("This sentence does not have 2 or more words to swap");




        System.out.println("\n----------TASK-3----------");


        String str1 = ScannerHelper.getString();

        if(str1.toLowerCase().contains("Stupid") || str1.toLowerCase().contains("idiot")){
            System.out.println(str1.replace("stupid", "nice").replace("idiot", "nice"));
        }
        else System.out.println(str1);


        System.out.println("\n----------TASK-4----------");


        String name = ScannerHelper.getFirstName();

        if(name.length() < 2){
            System.out.println("INVALID INPUT!!!");
        }
        else if(name.length() >= 2 && name.length() % 2 == 0) {
            System.out.println("" + name.charAt(name.length()/2 -1) + name.charAt(name.length()/2));
        } else System.out.println(name.charAt(name.length()/2));




        System.out.println("\n----------TASK-5----------");


        String country = ScannerHelper.getFavCountry();

        if(country.length() < 5) {
            System.out.println("INVALID INPUT!!!");
        }
        else System.out.println(country.substring(2, country.length()-2));


        System.out.println("\n----------TASK-6----------");


        String address = ScannerHelper.getString();

        System.out.println(address.toLowerCase().replace("a", "*").replace("e", "#")
                .replace("i", "+").replace("o", "@").replace("u", "$"));


        System.out.println("\n----------TASK-7----------");

        String fullSentence = ScannerHelper.getString();

        int count = 0;
        for (int i = 0; i < fullSentence.length(); i++) {
           char ch = fullSentence.charAt(i);

            if (ch == ' ')
                count++;
           System.out.println("This sentence has " + count + " words");
        }


    }


}