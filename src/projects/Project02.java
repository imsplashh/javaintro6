package projects;

import java.util.Scanner;

public class Project02 {
    public static void main(String[] args) {

        System.out.println("\n----------TASK-1----------");

        Scanner input = new Scanner(System.in);

        System.out.println("Please enter 3 numbers");
        int num1 = input.nextInt();
        int num2 = input.nextInt();
        int num3 = input.nextInt();

        System.out.println("The product of the numbers entered is=" + num1 * num2 * num3);



        System.out.println("\n----------TASK-2----------");

        System.out.println("What is your first name?");
        String first = input.next();

        System.out.println("What is your last name?");
        String last = input.next();

        System.out.println("What is your year of birth?");
        int year = input.nextInt();


        System.out.println(first + "  " + last+"'s" + "age is =" +(2023-year));



        System.out.println("\n----------TASK-3----------");

        System.out.println("What is your full name?");
        String fName = input.nextLine();

        System.out.println("What is your weight?");
        int weight = input.nextInt();

        System.out.println(fName+"'s " + "weight is = " + weight * 2.205 + " lbs.");



        System.out.println("\n----------TASK-4----------");


        System.out.println("What is your full name?");
        String fullName1 = input.nextLine();

        System.out.println("What is your age?");
        int age1 = input.nextInt();
        input.nextLine();

        System.out.println("What is your full name?");
        String fullName2 = input.nextLine();

        System.out.println("What is your age?");
        int age2 = input.nextInt();
        input.nextLine();

        System.out.println("What is your full name?");
        String fullName3 = input.nextLine();

        System.out.println("What is your age?");
        int age3 = input.nextInt();
        input.nextLine();

       int average = (age1 + age2 +age3) /3;

     int old = Integer.max(age1, age2);
     int eldest = Integer.max(old,age3);

     int young = Integer.min(age1,age2);
     int youngest = Integer.min(young,age3);




        System.out.println(fullName1+"'s" + "age is " +age1);
        System.out.println(fullName2+"'s" + "age is " +age2);
        System.out.println(fullName3+"'s" + "age is " +age3);
        System.out.println("The average age is " + average);
        System.out.println("The eldest age is " + eldest);
        System.out.println("The youngest age is " + youngest);




    }
}
