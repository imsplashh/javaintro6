package projects;

import utilities.ScannerHelper;

import java.util.Random;

public class Project05 {
    public static void main(String[] args) {

        System.out.println("\n----------TASK-1----------");

        String sentence = ScannerHelper.getString();

        int words = 1;

        if (sentence.contains(" ")) {
            words++;
            System.out.println("This sentence has " + words + " words");
        } else System.out.println("This sentence does not have multiple words.");


        System.out.println("\n----------TASK-2----------");

        Random num = new Random();

        int num1 = num.nextInt(25);
        int num2 = num.nextInt(25);

        System.out.println(num1);
        System.out.println(num2);

        System.out.println("\n============= print ==============");


        for (int i = Math.min(num1, num2); i <= Math.max(num1, num2); i++) {
            if (!(i % 5 == 0)) System.out.println(i);
        }


        System.out.println("\n----------TASK-3----------");

        String sentence1 = ScannerHelper.getString();

        int countOfA = 0;

        for (int i = 0; i <= sentence1.length() - 1; i++) {
            if (sentence1.length() - 1 == 0) System.out.println("This sentence does not have any characters");
            else if (sentence1.toLowerCase().charAt(i) == 'a') countOfA++;

        }
        System.out.println("This sentence has " + countOfA + " a or A letters");


        System.out.println("\n----------TASK-4----------");


        String word = ScannerHelper.getString();

        String reverse = "";
        char ch;


        for (int i = 0; i < word.length(); i++) {
            ch = word.charAt(i);
            reverse = ch + reverse;

            if (word.length() == 0) System.out.println("This word does not have 1 or more characters");

            else if (word.equals(reverse)) System.out.println("This word is palindrome");

            else System.out.println("This word is palindrome");

        }


        System.out.println("\n----------TASK-5----------");

        int rows = 5, k = 0;

        for (int i = 1; i <= rows; ++i, k = 0) {
            for (int space = 1; space <= rows - i; ++space) {
                System.out.print("  ");
            }

            while (k != 2 * i - 1) {
                System.out.print("* ");
                ++k;
            }

            System.out.println();


        }
    }
}
