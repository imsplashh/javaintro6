package character_class;

import utilities.ScannerHelper;

public class CountCharacters {
    public static void main(String[] args) {

        System.out.println("\n----------TASK-1----------");

        /*
        Write a program that asks user for a string
        Count letters
        Count digits

         */


        String str = ScannerHelper.getString();

        int digits = 0;
        int letters = 0;

        for (int i = 0; i <= str.length()-1; i++) {
            if(Character.isDigit(str.charAt(i))) digits++;
            else if(Character.isLetter(str.charAt(i))) letters++;
        }

        System.out.println("The string has " + digits + " digits and " + letters + " letters");

        System.out.println("\n----------TASK-2----------");


        String str1 = ScannerHelper.getString();

        int upper = 0;
        int lower = 0;

        for (int i = 0; i <= str1.length()-1; i++) {
            if(Character.isUpperCase(str1.charAt(i))) upper++;
           else if(Character.isLowerCase(str1.charAt(i)))  lower++;

        }


        System.out.println("\n----------TASK-3----------");


        /*
        Write a program that asks user for a string
        count all special characters
         */

        String str3 = ScannerHelper.getString();

        int special  = 0;

        for (int i = 0; i <= str3.length()-1; i++) {
            char c = str3.charAt(i);
            if(!Character.isDigit(c) && !Character.isLetter(c) && !Character.isSpaceChar(c)) special++;
        }

        System.out.println(special);



        System.out.println("\n----------TASK-4----------");

        /*
        write a program asking user for a string

         */

        int letters1 = 0;
        int upperCase1 = 0;
        int lowerCase1 = 0;
        int digits1 = 0;
        int spaces = 0;
        int special1 = 0;

        String str4 = ScannerHelper.getString();

        for (int i = 0; i <= str4.length()-1; i++) {
            char b = str4.charAt(i);
            if(Character.isLetter(b)){
                letters1++;
                if(Character.isUpperCase(b)) upperCase1++;

                else if(Character.isLowerCase(b)) lowerCase1++;

            }
           else if(Character.isDigit(b)) digits1++;
           else if(Character.isSpaceChar(b)) spaces++;
           else if(!Character.isDigit(b) && !Character.isLetter(b) && !Character.isSpaceChar(b)) special1++;

        }
        System.out.println("Letters = " + letters1);
        System.out.println("Uppercase letters = " + upperCase1);
        System.out.println("Lowercase letters = " + lowerCase1);
        System.out.println("Digits = " + digits1);
        System.out.println("Spaces = " + spaces);
        System.out.println("Specials = " + special1);



    }
}
