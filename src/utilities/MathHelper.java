package utilities;

import java.util.Random;

public class MathHelper {

    //Write a public static method named sum() and returns the sum of 2 numbers

    public static int sum(int a, int b) {

        return a + b;
    }

    public static double sum(double a, double b) {

        return a + b;
    }

    public static int sum(int a, int b, int c) {

        return (a + b) + c;
    }

    //Write a public static method named product() and returns the product of 2 numbers

    public static int product(int a, int b) {

        return a * b;
    }

    //Write a public static method named square() and returns the square of a number

    public static int square(int a) {

        return a * a;
    }


    public static int max(int num1, int num2, int num3) {

        return Math.max(Math.max(num1, num2), num3);


    }




}


