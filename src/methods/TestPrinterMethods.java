package methods;

import utilities.MathHelper;
import utilities.Printer;
import utilities.RandomGenerator;

public class TestPrinterMethods {
    public static void main(String[] args) {

        Printer.printGM();
        Printer.printGM();

        Printer.helloName("Joe");


        //static vs non-static
        //static can be invoked with the class name
        //non-static


        Printer myPrinter = new Printer();
        myPrinter.printTechGlobal();

        MathHelper.sum(3, 5);
        Printer.printGM();



    }
}
