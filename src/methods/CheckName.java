package methods;

import utilities.ScannerHelper;

public class CheckName {
    public static void main(String[] args) {




        String name = ScannerHelper.getFirstName();
        String lastName = ScannerHelper.getLastName();

        System.out.println("The full name entered is = " + name + " " + lastName);

        int age = ScannerHelper.getAge();

        System.out.println("The age entered by the user is = " +age);


    }
}
