package loops;

public class Excercise04_PrintNumbersDivisibleBy5 {
    public static void main(String[] args) {


        /*
        write a program that checks numbers from 1 to 50 and print all numbers that can be divided by 5
         */
        for(int i = 1; i <= 50; i++){
            if(i % 5 == 0) System.out.println(i);
        }

    }
}
