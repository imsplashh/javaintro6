package loops.practices;

import utilities.ScannerHelper;

public class Excercise01 {
    public static void main(String[] args) {

        System.out.println("\n----------TASK-1----------");

        /*
        Write a program that prints all the numbers from 1 to 10
        (1 and 10 are included)
        If the number is dividable by 2, then print “Foo” instead of
        number itself
        If the number is dividable by 5, then print “Bar” instead
        of number itself
        If the number is dividable by both 2 and 5, then print
        “FooBar” instead of number itself
        Else print number itself
         */

        for (int i = 1; i <= 10; i++) {
            if(i % 2 == 0 && i % 5 == 0) System.out.println("FooBar");
            else if(i % 2 == 0) System.out.println("Foo");
            else if(i % 5 == 0) System.out.println("Bar");
            else System.out.println(i);

        }


    }
}
