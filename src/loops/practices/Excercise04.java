package loops.practices;

import utilities.ScannerHelper;

public class Excercise04 {
    public static void main(String[] args) {

        /*
        Write a program that asks user to enter a String.
        Count the number of vowels in the given String and print it.
        Vowels are A, E, O, U, I, a, e, o, u, I

         */

        String str = ScannerHelper.getString();
        int count = 0;

        for (int i = 0; i <= str.length()-1; i++) {
            char c = Character.toLowerCase(str.charAt(i));

            if(c == 'a' || c == 'e' || c == 'i' ||c == 'o' || c== 'u') count++;

        }
        System.out.println(count);



    }
}
