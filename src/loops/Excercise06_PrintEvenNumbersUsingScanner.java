package loops;

import utilities.ScannerHelper;

import java.util.Scanner;

public class Excercise06_PrintEvenNumbersUsingScanner {
    public static void main(String[] args) {

        /*
        write a program that asks a user to enter 2 different positive numbers
        Print all even numbers by given numbers
         */

        int first = ScannerHelper.getNumber();
        int second = ScannerHelper.getNumber();


        for(int i = Math.min(first,second); i <= Math.max(first,second); i++){
            if(i % 2 == 0 ) System.out.println(i);
        }


    }
}
