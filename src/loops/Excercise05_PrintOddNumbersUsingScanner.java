package loops;

import utilities.ScannerHelper;

public class Excercise05_PrintOddNumbersUsingScanner {
    public static void main(String[] args) {

        /*
        write a program that asks user to enter a number print all odd numbers starting from  0
         */

        int number = ScannerHelper.getNumber();

        for (int i = 0; i <= number ; i++) {
            if(i % 2 == 1) System.out.println(i);
        }

    }
}
