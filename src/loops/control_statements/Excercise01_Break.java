package loops.control_statements;

import utilities.ScannerHelper;

public class Excercise01_Break {
    public static void main(String[] args) {

        int n1 = ScannerHelper.getNumber();
        int n2 = ScannerHelper.getNumber();

        for (int i = Math.max(n1,n2); i >= Math.min(n1,n2); i--) {
            if(i < 10) break;
            System.out.println(i);
        }


    }
}
