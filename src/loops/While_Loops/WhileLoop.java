package loops.While_Loops;

public class WhileLoop {
    public static void main(String[] args) {

        //print numbers 1 - 5
        System.out.println("\n-----------for loop-----------\n");

        for (int i = 1; i <= 5; i++) {
            System.out.println(i);
        }

        System.out.println("\n-----------While loop-----------\n");

        int  num = 1;

        while(num <= 5){
            System.out.println(num);
            num++;
        }



    }
}
