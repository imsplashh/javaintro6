package loops.While_Loops;

public class Excercise01_PrintNumbersDividedBy3 {
    public static void main(String[] args) {

        /*
        write  a program that prints all the numbers divided by 3, starting from 1 to 100 included
         */

        int num = 1;

        while(num <= 100){
            if(num % 3 == 0 )System.out.println(num);
            num++;
        }


    }
}
