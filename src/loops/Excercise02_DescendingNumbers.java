package loops;

public class Excercise02_DescendingNumbers {
    public static void main(String[] args) {

        /*

        print number 100 down to 0
         */

        for(int i = 100; i >= 0; i--){
            System.out.println(i);
        }


    }
}
