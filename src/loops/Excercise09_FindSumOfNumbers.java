package loops;

public class Excercise09_FindSumOfNumbers {
    public static void main(String[] args) {

        /*
        write a program to find sum of numbers from 10 - 15 included
         */

        int sum = 0;

        for (int i = 10; i <= 15 ; i++) {
            sum += i;

        }
        System.out.println(sum);
    }
}
