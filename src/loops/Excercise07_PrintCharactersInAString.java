package loops;

import utilities.ScannerHelper;

public class Excercise07_PrintCharactersInAString {
    public static void main(String[] args) {

        /*
        Write a program that asks a user to enter a string
        Print  each character of the string in a separate line
         */

        String str = ScannerHelper.getString();

        for (int i = 0; i <= str.length()-1; i++) {
            System.out.println(str.charAt(i));
        }


    }
}
