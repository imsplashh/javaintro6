package loops;

import utilities.ScannerHelper;

public class Excercise12_ReverseString {
    public static void main(String[] args) {

        /*
         Write a program that reads a name from user
        Reverse the name and print it back
         */

        String name = ScannerHelper.getFirstName();

        String reverse = "";
        char ch;

        for (int i = 0; i < name.length() ; i++) {
            ch = name.charAt(i);
            reverse = ch+reverse;
        }
        System.out.println(reverse);

        System.out.println("\n======Method 2 ========\n");

        String name2 = ScannerHelper.getFirstName();

        String reverseName = "";

        for (int i = name2.length()-1; i >= 0 ; i--) {
            reverseName += name.charAt(i);
        }
        System.out.println(reverseName);




    }
}
