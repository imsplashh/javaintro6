package loops;

import utilities.ScannerHelper;

public class Excercise10_SumOfNumbersByUser {
    public static void main(String[] args) {

        /*
        write a program that asks user to enter 5 numbers
        and find sum
         */


        int num1 = ScannerHelper.getNumber();
        int num2 = ScannerHelper.getNumber();
        int num3 = ScannerHelper.getNumber();
        int num4 = ScannerHelper.getNumber();
        int num5 = ScannerHelper.getNumber();

       int sum1 = num1 + num2 + num3 + num4 + num5;

        System.out.println(sum1);


        System.out.println("\n ------------with for loop ----------------");

        int sum2 = 0;

        for (int i = 1; i <= 5; i++) {

            sum2 += ScannerHelper.getNumber();
        }
        System.out.println(sum2);


        System.out.println("\n ------------with while loop ----------------");


        int start = 1;
        int sum3 = 0;

        while (start <= 5){
           sum3 += ScannerHelper.getNumber();
            start++;
        }
        System.out.println(sum3);







    }
}
