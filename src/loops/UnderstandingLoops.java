package loops;

public class UnderstandingLoops {
    public static void main(String[] args) {

        /*
        for loop su = yntax

        for(initialization; termination; update){
        //block of  code
        }

        initialization -> start point
        termination -> stop condition
        update -> increasing or decreasing operators

         */


        //print Hello World! 5 times

        for(int num = 0; num < 20; num++){
            System.out.println("Hello World!");
        }



    }
}
