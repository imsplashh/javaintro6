package loops;

public class Excercise03_PrintEvenNumbers {
    public static void main(String[] args) {

        /*
        print  even numbers only from 0-10
         */

        for(int i = 0; i <= 10; i++ ){
            if(i % 2 == 0) System.out.println(i);
        }

    }
}
