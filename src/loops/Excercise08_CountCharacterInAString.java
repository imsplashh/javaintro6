package loops;

import utilities.ScannerHelper;

public class Excercise08_CountCharacterInAString {
    public static void main(String[] args) {

        /*
        write a program that asks user to enter a string
        Count how many A or a letter you have in given string
         */

        String str = ScannerHelper.getString();
        int count = 0;

        for (int i = 0; i <= str.length()-1; i++) {
            if(str.charAt(i) == 'A' || str.charAt(i) == 'a') count++;

        }
        System.out.println(count);


    }
}
