package loops.do_while_loops;

import utilities.ScannerHelper;

public class Excercise01_GetNumberDividedBy5 {
    public static void main(String[] args) {

        /*
        write a program that asks user to enter a number
        and keep asking until they enter  a number that can be entered by 5.
         */


        int number;
        do {
            number = ScannerHelper.getNumber();
        } while (number % 5 != 0);

        System.out.println("End of the program");


        System.out.println("\n-----------for loop----------\n");
        //Create an infinite loop, keep asking user to enter a number but break the loop when you got a number that can be divided by 5

        for (; ;) {
            int n2 = ScannerHelper.getNumber();
            if(n2 % 5 == 0) {
                System.out.println("End of the program");
                break;
            }
        }

    }
}
