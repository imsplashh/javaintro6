package date_time;

import utilities.ScannerHelper;

import java.time.LocalDate;
import java.util.Scanner;

public class CalculateAge {
    public static void main(String[] args) {

        LocalDate currentDate = LocalDate.now();

        Scanner input = new Scanner(System.in);
        System.out.println("Please enter your date of birth");

        int age = input.nextInt();

        System.out.println(currentDate.getYear() - age);



    }
}
