package data_time;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;

public class _01_LocalDateTime {
    public static void main(String[] args) {

        System.out.println("\n----------LocalDate-----------\n");
        LocalDate currentDate = LocalDate.now();

        System.out.println(currentDate); // 2023-05-02 yyyy-MM-dd

        System.out.println(currentDate.format(DateTimeFormatter.ofPattern("MM/dd/yyyy")));

        System.out.println(currentDate.plusDays(5));


        System.out.println("\n----------LocalTime-----------\n");
        LocalTime currentTime = LocalTime.now();

        System.out.println(currentTime); // 18:14:32.855 hh:mm:ss.SSS

        System.out.println(currentTime.plusHours(6).format(DateTimeFormatter.ofPattern("hh:mm:ss: a")));// hh:mm:ss: a


        System.out.println("\n----------LocalDateTime-----------\n");
        LocalDateTime currentDateTime = LocalDateTime.now();

        System.out.println(currentDateTime); // 2023-05-02T18:16:11.107 yyyy-MM-ddThh:mm:ss.SSS

        System.out.println(currentDateTime.format(DateTimeFormatter.ofPattern("MMM d, yyyy h:mm a")));

        System.out.println(Calendar.getInstance().getTime());


    }
}