package operators.shorthand_operators;

import java.util.Scanner;

public class Excercise02 {
    public static void main(String[] args) {

        Scanner inputReader = new Scanner(System.in);

        System.out.println("Please enter your balance: ");
        double balance = inputReader.nextDouble();

        System.out.println("The initial balance $ =" + balance);

        System.out.println("What is the first transaction amount?");
        double firstTrns = inputReader.nextDouble();

        balance -= firstTrns;

        System.out.println("The balance after first transaction = $" + balance);

        System.out.println("What is the second transaction amount?");
        System.out.println("The balance after the second transaction =  $" + (balance -= inputReader.nextDouble()));

        System.out.println("What is the third transaction amount?");
        System.out.println("The balance after the third transaction =  $" + (balance -= inputReader.nextDouble()));





    }
}
