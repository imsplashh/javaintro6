package operators.shorthand_operators;

public class ShorthandPractice {
    public static void main(String[] args) {

        int age = 45;

        System.out.println("Age in 2023 = " + age);

        //age = age + 5;
        age += 5;

        System.out.println("Age in 2028 = " + age);

        age -= 20;
        System.out.println(age);

        age /= 3;
        System.out.println(age);

        age %= 4;
        System.out.println(age);

    }
}
