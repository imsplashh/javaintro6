package operators.increment_decrement_operators;

public class PostIncrementPostDecrement {
    public static void main (String[] args){

        System.out.println("\n----------TASK-1----------");


        int num1 = 10, num2 = 10;

        System.out.println(num1++);
        System.out.println(num1);
        System.out.println(num1);

        System.out.println(++num2);
        System.out.println(num2);


        System.out.println("\n----------TASK-2----------");


        int n1 = 5, n2 = 7;

        System.out.println(n1++);
        System.out.println(n1 += n2);

        System.out.println(n1);


        System.out.println("\n----------TASK-3----------");


        int i1 = 10;

        System.out.println(--i1);

        System.out.println(i1--);

        System.out.println(--i1);


        System.out.println("\n----------TASK-4----------");


        int number1 = 50;

        number1 -= 25;

        number1 -= 10;

        System.out.println(number1--);


        System.out.println("\n----------TASK-5----------");


        int i = 5;

        int age = 10 * i++;

        System.out.println(age);

        System.out.println("\n----------TASK-6----------");

        int var1 = 27;

        int result = --var1 / 2;

        System.out.println(++result);




    }


}
