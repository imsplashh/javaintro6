package operators.arithmetic_operators;

public class MathFunctions {
    public static void main(String[] args) {
        int num1 = 9, num2 = 3;


        System.out.println(num1 + num2);

        //multiply
        System.out.println(num1 * num2);

        //subtract
        System.out.println(num1 - num2);

        //division
        System.out.println(num1 / num2);

        //remainder
        System.out.println(num1 % num2);

        int sum = num1 + num2;
        int subtraction = num1 - num2;
        int multiplication = num1 * num2;
        int division = num1 / num2;
        int remainder = num1 % num2;

        System.out.println("The sum of " + num1 + " and " + num2 + " = " + sum);
        System.out.println("The subtraction of " + num1 + " and " + num2 + " = " + subtraction);
        System.out.println("The multiplication of " + num1 + " and " + num2 + " = " + multiplication);
        System.out.println("The division of " + num1 + " and " + num2 + " = " + division);
        System.out.println("The remainder of " + num1 + " and " + num2 + " = " + remainder);



    }
}
