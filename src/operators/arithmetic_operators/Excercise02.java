package operators.arithmetic_operators;

public class Excercise02 {
    public static void main(String[] args) {

        double averageSalary = 120000;

        //Monthly
        System.out.println("Monthly = $" + averageSalary / 12);

        //Weekly
        System.out.println("Weekly = $" + averageSalary / 52);

        //Bi-weekly
        System.out.println("Bi-weekly = $" + averageSalary / 26);


    }
}
