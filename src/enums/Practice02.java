package enums;

public class Practice02 {
    public static void main(String[] args) {

        Frequency frequency = Frequency.MONTHLY;

        switch (frequency) {
            case HOURLY:
                System.out.println("$20 per hour");
                break;
            case DAILY:
                System.out.println("$160 per day");
                break;
            case WEEKLY:
                System.out.println("$800 per week");
                break;
            case BI_WEEKLY:
                System.out.println("$1600 bi-weekly");
                break;
            case MONTHLY:
                System.out.println("$3200 per month");
                break;
            case YEARLY:
                System.out.println("$38400 per year");
                break;
            default:
                System.out.println("Invalid time period");
                break;
        }
    }
}



