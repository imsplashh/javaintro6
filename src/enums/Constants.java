package enums;

public class Constants {

    // we can make multiple enums inside a class

    public enum Gender{
        FEMALE,
        MALE,
        OTHER

    }

    public enum Directions{
        NORTH,
        SOUTH,
        EAST,
        WEST

    }
}
