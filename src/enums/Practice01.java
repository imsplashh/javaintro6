package enums;

public class Practice01 {
    public static void main(String[] args) {

        for (DaysOfTheWeek day : DaysOfTheWeek.values()) {
            if (day == DaysOfTheWeek.SATURDAY || day == DaysOfTheWeek.SUNDAY) {
                System.out.println(day + " is not a work day");
            } else {
                System.out.println(day + " is a work day");
            }
        }
    }
}
