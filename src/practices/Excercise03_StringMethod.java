package practices;

import utilities.ScannerHelper;

public class Excercise03_StringMethod {
    public static void main(String[] args) {

       String str = ScannerHelper.getString();

       if(str.length() < 4) System.out.println("INVALID INPUT");

       else if(str.length() >= 4){
           System.out.println(str.substring(0,2));
           System.out.println(str.substring(str.length()-2));
           System.out.println(str.substring(2, str.length()-2));
       }







    }
}
