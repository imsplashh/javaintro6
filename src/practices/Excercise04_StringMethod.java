package practices;

import utilities.ScannerHelper;

public class Excercise04_StringMethod {
    public static void main(String[] args) {

       String input = ScannerHelper.getString();

        if(input.length() >= 2) System.out.println(input.substring(0,2).equals(input.substring(input.length()-2)));
        else System.out.println("Length is less than 2");



    }
}
