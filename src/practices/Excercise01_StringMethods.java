package practices;

import utilities.ScannerHelper;

import java.sql.SQLOutput;

public class Excercise01_StringMethods {
    public static void main(String[] args) {

        System.out.println("\n----------TASK-1----------");

        String str = ScannerHelper.getString();

        System.out.println("The string given is = " + str);




        System.out.println("\n----------TASK-2----------");

        int str1 = str.length();


        if(str1 == 0){
            System.out.println("The string given is empty");
        }
        else{
            System.out.println(str1);
        }




        System.out.println("\n----------TASK-3----------");

        if(!str.isEmpty()){
            System.out.println("The first character = " + str.charAt(0));
        }
        else if(str.isEmpty()){
            System.out.println("There is no character in this string");
        }


        System.out.println("\n----------TASK-4----------");


        char char1 = str.charAt(str.length()-1);

        if(!str.isEmpty()){
            System.out.println("The last character = " + char1);
        }
        else {
            System.out.println("There is no character in this string");
        }



        System.out.println("\n----------TASK-5----------");


        if(str.contains("a") || str.contains("e") || str.contains("i") || str.contains("o") ||
                str.contains("u") || str.contains("A") || str.contains("E") || str.contains("I") ||
                str.contains("O") || str.contains("O")){

            System.out.println("This string has a vowel");
        }
        else{
            System.out.println("This string does not have a vowel");
        }











    }
}
