package gitDemo;

public class Branch {
    public static void main(String[] args) {
        /*
        Branching: We create branches  for individual members, so they can get started with coding
            example: feature/teamname/userstory#-context
                    feature/AutomationTester/US1307-Retry-Functionality

           git branch nameofthebranch
           git checkout -b nameofthebranch

           Task-01 - Create a new branch called feature/Javateam/US01-learningaboutmergeconflicts
         // new line of code
           Task02 - Added a  new line
         */


    }
}
