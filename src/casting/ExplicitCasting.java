package casting;

public class ExplicitCasting {
    public static void main(String[] args) {
        long number1 = 128;

        byte number2 = (byte) number1;
        System.out.println(number2);
    }
}
