package casting;

public class WrapperClasses {
    public static void main(String[] args) {

        int age1 = 45;

        Integer age2 = 45;

        System.out.println(age1);
        System.out.println(age2);

        System.out.println(Integer.MAX_VALUE);
        System.out.println(Double.MIN_VALUE);
        System.out.println(Short.MAX_VALUE);

        String s = "25.5";

        System.out.println(Double.parseDouble(s) + 5);

        System.out.println((int) Double.parseDouble(s) + 5);

        String str = "12345";

        System.out.println(Integer.parseInt(str) + 5);

        char c1 = 'A';
        Character c3 = 'E';
        char c4 = c3;






    }
}
