package collections;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class _01_List {
    public static void main(String[] args) {
        /*
        List is an interface, and it has some class implementation

        1. ArrayList - List that stores objects, Most common implementation of list interface, better if data wont be changed a lot
        - better for storing and accessing data
        2. LinkedList - the way it stores element is different from Arraylist (node), better if data will be changed a lot
       - easily manipulated
        3. Vector - synchronized list

        Common features:
        - They keep insertion order
        - They allow null elements
        - They allow duplicates





         */

        ArrayList<String> list = new ArrayList<>();
        list.add("Carmela");
        list.add("Okan");
        list.add(null);


        System.out.println(list);

        System.out.println("\n-----------ArrayList and LinkedList in the shape of List---------\n");
        List<Integer> numbers1 = new ArrayList<>();
        List<Integer> numbers2 = new LinkedList<>();


    }

}
