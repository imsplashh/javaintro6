package collections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public class Exercise01_RemoveDuplicates {
    public static void main(String[] args) {

        System.out.println(removedDuplicates(new ArrayList<>(Arrays.asList(2, 3, 4 , 5, 6, 6, 7, 7))));

        
        
    }
    
    public static HashSet<Integer> removedDuplicates(List<Integer> nums){
        return new HashSet<>(nums);

    }
}
