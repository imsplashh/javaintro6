package string_Methods;

import java.util.Scanner;

public class ExcerciseCharAt {
    public static void main(String[] args) {

        String str = "TechGlobal"; //last index is 9 | Length is 10
        String str2 = "Hello World"; //last index is 10 | Length is 11
        String str3 = "I really love java"; //last index is 17 | Length is 18

        System.out.println(str.charAt(4));

        //last character

        System.out.println(str.charAt(9));
        System.out.println(str2.charAt(9));
        System.out.println(str3.charAt(9));

        System.out.println("\n Way 2 \n");

        //Method 2
        System.out.println(str.charAt(str.length()-1));
        System.out.println(str2.charAt(str2.length()-1));
        System.out.println(str3.charAt(str3.length()-1));


        Scanner input = new Scanner(System.in);
        System.out.println("Please enter a String");
        String string1 = input.nextLine();

        System.out.println(string1.charAt(string1.length()-1));





    }
}
