package string_Methods;

public class ExcerciseGetCharOrChars {
    public static void main(String[] args) {

        String name1 = "Jehad";
        String name2 = "Bilal";
        String name3 = "Ronaldo!!";

        //length of an odd word is length()-1)/2;

        System.out.println(name1.charAt((name1.length()-1)/2));
        System.out.println(name1.charAt((name2.length()-1)/2));
        System.out.println(name1.charAt((name3.length()-1)/2));


        // length of even words

        String name4 = "yousef";

        System.out.println("" + name4.charAt(name4.length()/2 -1) + name4.charAt(name4.length()/2));






    }
}
