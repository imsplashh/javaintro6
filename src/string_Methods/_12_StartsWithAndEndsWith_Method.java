package string_Methods;

import java.util.Scanner;

public class _12_StartsWithAndEndsWith_Method {
    public static void main(String[] args) {

        String str = "TechGlobal";

        boolean startsWith = str.startsWith("T");
        boolean endsWith = str.endsWith("T");

        System.out.println(startsWith);
        System.out.println(endsWith);





    }
}
