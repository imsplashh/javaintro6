package string_Methods;

public class _08_IndexOf_Method {
    public static void main(String[] args) {

        /*
        1. return type
        2. int
        3. non-static
        4. takes the string or char as argument
         */

        String str = "TechGlobal";

        System.out.println(str.indexOf("h")); //3

        System.out.println(str.indexOf("Tech")); // 0

        System.out.println(str.indexOf("Global")); // 4

        System.out.println(str.indexOf("l")); // 5

        // lastIndexOf()

        System.out.println(str.lastIndexOf("l")); // 9






    }
}
