package string_Methods;

public class _04_EqualsIgnoreCase_Method {
    public static void main(String[] args) {

        //ABC
        //abc

        String str1 = "Hello";
        String str2 = "Hi";
        String str3 = " hello";
        String str4 = "HeLlOHI";

        System.out.println(str1.equalsIgnoreCase(str2));

        System.out.println(str1.equalsIgnoreCase(str3));

        System.out.println("123".equalsIgnoreCase("123"));





    }
}
