package string_Methods;

public class _06_ToUpperCase_Method {
    public static void main(String[] args) {

        /*
        1. Return type
        2. Returns a String
        3. Non-static
        4. no arguments
         */

        System.out.println("HelloWorld".toUpperCase());

        String s1 = "HELLO";
        String s2 = "HELLO";

        if(s1.toUpperCase().equals(s2.toUpperCase())) System.out.println("EQUAL");
        else System.out.println("NOT EQUAL");



    }
}
