package string_Methods;

public class _05_ToLowerCase_Method {
    public static void main(String[] args) {

        /*
        1. return type -> String
        2. returns a string
        3. non-static
        4. does not take arguments
         */
        String str1 = "JAVA IS FUN";
        String str2 = "java is fun";

        System.out.println(str1);
        System.out.println(str1.toLowerCase());

        System.out.println(str2.toLowerCase());

        char c = 'A';

        System.out.println(String.valueOf(c).toLowerCase());





    }
}
