package string_Methods;

import java.util.Scanner;

public class Excercise05 {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);


        System.out.println("Please enter a word");
        String word = input.nextLine();

        boolean startsWith = word.startsWith("a") || word.startsWith("A");
        boolean endsWith = word.endsWith("e") || word.endsWith("E");

        System.out.println(startsWith && endsWith);


    }
}
