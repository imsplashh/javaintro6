package string_Methods;

public class _03_Equals_Method {
    public static void main(String[] args) {

        /*
        1. return type or void -> return
        2. Whats returning -> boolean
        3. Static or Non-static -> non-static
        4. takes any variable as an argument
         */


        String str1 = "Tech";
        String str2 = "Global";
        String str3 = "tech";
        String str4 = "TechGlobal";
        String str5 = "Tech";

        boolean isEquals = str1.equals(str2);
        System.out.println(isEquals);

        System.out.println(str1.equals(str3));

        System.out.println(str1.concat(str2).equals(str4));

        System.out.println(!str1.equals(str5));






    }
}
