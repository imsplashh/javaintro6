package string_Methods;

public class _07_CharAt_Method {
    public static void main(String[] args) {


        /*
        1. return type
        2. char
        3. non-static
        4. yes it takes an int index as an argument
         */
        String name = "Jehad";

        System.out.println(name.charAt(0));
        System.out.println(name.charAt(1));
        System.out.println(name.charAt(2));
        System.out.println(name.charAt(3));
        System.out.println(name.charAt(4));

        String str = "Hello World";
        System.out.println(str.charAt(9));
        System.out.println(str.charAt(10));
       // System.out.println(str.charAt(-3));

       // String s = "";
       // System.out.println(s.charAt(0));





    }
}
