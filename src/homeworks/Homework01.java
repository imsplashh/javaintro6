package homeworks;

import static javafx.scene.input.KeyCode.J;

public class Homework01 {
    public static void main(String[] args) {

        System.out.println("\n----------TASK-1----------");

        /*

        128  64   32  16   8   4  2  1

        JAVA = 74 65 86 65
         0100101, 01000001, 01010110, 01000001
         0100101010000010101011001000001

        SELENIUM = 83 69 76 69 78 73 85 77
        01010011, 01000101, 1001100, 01000101, 01001110, 01001001, 01010101, 01001101
        010100110100010110011000100010101001110010010010101010101001101

         */


        System.out.println("\n----------TASK-2----------");

        /*

        01001101    = Decimal Val. = 77 Letter = M
        01101000    = Decimal Val. = 104 Letter = h
        01111001    = Decimal Val. = 121 Letter = y
        01010011    = Decimal Val. = 83 Letter = S
        01101100    = Decimal Val. = 108 Letter = l
         */


        System.out.println("\n----------TASK-3----------");

        System.out.println("I start to practice \"JAVA\" today, and I like it.\n");

        System.out.println("The secret of getting ahead is getting started.\n");

        System.out.println("\"Don't limit yourself.\"\n");

        System.out.println("Invest in your dreams. Grind now. Shine later.\n");

        System.out.println("It’s not the load that breaks you down, it’s the way you carry it.\n");

        System.out.println("The hard days are what make you stronger.\n");

        System.out.println("You can waste your lives drawing lines. Or you can live your life crossing them.\n");


        System.out.println("\n----------TASK-4----------");

        System.out.println("\tJava is easy to write and easy to run—this is the foundational \n strength of Java and why many developers program in it. When you \n write Java once, you can run it almost anywhere at any time.\n\n" +
                "\n" +
                "\tJava can be used to create complete applications that can run on \n a single computer or be distributed across servers and clients in a \n network.\n\n" +
                "\n" +
                "\tAs a result, you can use it to easily build mobile applications or \n run-on desktop applications that use different operating systems and \n servers, such as Linux or Windows.\n");


        System.out.println("\n----------TASK-5----------");

        int myAge = 24;
        System.out.println(myAge);

        int myFavoriteNumber = 15;
        System.out.println(myFavoriteNumber);

        double myHeight = 5.8;
        System.out.println(myHeight);

        int myWeight = 170;
        System.out.println(myWeight);

        char myFavoriteLetter = 'J';
        System.out.println(myFavoriteLetter);




    }
}
