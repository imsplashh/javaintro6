package homeworks;

import java.util.ArrayList;
import java.util.Arrays;

public class Homework09 {
    public static void main(String[] args) {

        System.out.println("========== TASK 1 ==========");

        System.out.println(firstDuplicatedNumber(new int[]{2, 4, 3, 3, 5, 5, 6, 6, 7}));


        System.out.println("========== TASK 2 ==========");

        System.out.println(firstDuplicatedString(new String[]{"Z", "abc", "z", "123", "#"}));

        System.out.println("========== TASK 3 ==========");

        System.out.println(duplicatedNumbers(new int[]{0, -4, -7, 0, 5, 10, 45, -7, 0}));

        System.out.println("========== TASK 4 ==========");

        System.out.println(duplicatedStrings(new String[]{"A", "foo", "12", "Foo", "bar", "a", "a", "java"}));

        System.out.println("========== TASK 5 ==========");

        System.out.println(Arrays.toString(reversedArray(new ArrayList<String>(Arrays.asList("abc", "foo", "bar")))));


        System.out.println("========== TASK 6 ==========");

        System.out.println(Arrays.toString(reversedStringWords(new ArrayList<>(Arrays.asList("Java is fun")))));





    }
    /*
    Write a method called as firstDuplicatedNumber to find the first duplicated number in an int array.
    NOTE: Make your code dynamic that works for any given int array and return -1 if there are no duplicates in the array.

     */

    // System.out.println("========== TASK 1 ==========");

    public static int firstDuplicatedNumber(int[] num) {


        for (int i = 0; i < num.length - 1; i++) {
            for (int j = i + 1; j < num.length; j++) {
                if (i != j && num[i] == num[j]) return num[i];
            }
        }
        return -1;

    }

    /*
    Write a method called as firstDuplicatedString to find the first duplicated String in a String array, ignore cases.
    NOTE: Make your code dynamic that works for any given String array and return “There is no duplicates” if there are no duplicates in the array.

     */
    //System.out.println("========== TASK 2 ==========");
    public static String firstDuplicatedString(String[] str) {


        for (int i = 0; i < str.length - 1; i++) {
            //String lowerI = str[i].toLowerCase();
            for (int j = i + 1; j < str.length; j++) {
                //String lowerJ = str[j].toLowerCase();
                if (str[i].equalsIgnoreCase(str[j])) return str[i];
            }
        }
        return "There is no duplicates";


    }
    /*
    Write a method called as duplicatedNumbers to find the all duplicates in an int array.
    NOTE: Make your code dynamic that works for any given int array and return empty ArrayList if there are no duplicates in the array.

     */
    //System.out.println("========== TASK 3 ==========");

    public static ArrayList<Integer> duplicatedNumbers(int[] num) {

        ArrayList<Integer> nums = new ArrayList<>();

        for (int i = 0; i < num.length - 1; i++) {
            for (int j = i + 1; j < num.length; j++) {
                if (num[i] == num[j] && !nums.contains(num[i])) nums.add(num[i]);
            }

        }
        return nums;
    }

    /*
    Write a method called as duplicatedStrings to find the all duplicates in a String array, ignore cases.
    NOTE: Make your code dynamic that works for any given String array and return and empty ArrayList if there are no duplicates in the array.

     */
    //System.out.println("========== TASK 4 ==========");

    public static ArrayList<String> duplicatedStrings(String[] arr) {

        ArrayList<String> str = new ArrayList<>();
        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[i].equalsIgnoreCase(arr[j])) {
                    if (!str.contains(arr[i]))
                        str.add(arr[i]);
                }
            }
        }
        return str;
    }

    //System.out.println("========== TASK 5 ==========");

        public static String[] reversedArray(ArrayList<String> str) {
            String[] rev = new String[str.size()];

            for (int i = 0; i < str.size(); i++) {
                String reversedStr = "";

                for (int j = str.get(i).length() - 1; j >= 0; j--) {
                    reversedStr += str.get(i).charAt(j);
                }

                rev[i] = reversedStr;
            }

            return rev;
        }



    /*
    Write a method called as reversedArray to reverse elements in an array, then print array.
    NOTE: Make your code dynamic that works for any given array.

     */
    //System.out.println("========== TASK 6 ==========");

    public static String[] reversedStringWords(ArrayList<String> str) {
        String[] rev = new String[str.size()];

        for (int i = 0; i < str.size(); i++) {
            String reversedStr = "";

            for (int j = str.get(i).length() - 1; j >= 0; j--) {
                reversedStr += str.get(i).charAt(j);
            }

            rev[i] = reversedStr;
        }

        return rev;
    }

}
