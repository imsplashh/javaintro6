package homeworks;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class Homework16 {
    public static void main(String[] args) {




        System.out.println("========= TASK 1 ==========");

        String inputData = "{104}LA{101}Paris{102}Berlin{103}Chicago{100}London";
        Map<Integer, String> parsedData = parseData(inputData);
        System.out.println(parsedData);

        System.out.println("========= TASK 2 ==========");
        Map<String, Integer> shoppingItems = new TreeMap<>();
        shoppingItems.put("Apple", 3);
        shoppingItems.put("Mango", 1);
        double totalPrice = calculateTotalPrice(shoppingItems);
        System.out.println("Total Price: $" + totalPrice);

        System.out.println("========= TASK 3 ==========");
        Map<String, Integer> items = new HashMap<>();
        items.put("Apple", 3);
        items.put("Mango", 5);
        double totalPrice2 = calculateTotalPrice2(items);
        System.out.println(totalPrice2);

    }





    /** System.out.println("========= TASK 1 ==========");
     * Requirement:
     * Write a method called as parseData() which takes a String has some keys in {} and values after between }{ and returns a collection that has all the keys and values as entries.
     * NOTE: The keys should be sorted!
     *
     * Test Data:
     * {104}LA{101}Paris{102}Berlin{103}Chicago{100}London
     *
     */


    public static Map<Integer, String> parseData(String data) {
        Map<Integer, String> result = new TreeMap<>();

        // Remove the leading "{" and trailing "}"
        data = data.replaceAll("[{}]", "");

        // Split the data string by "}{"
        String[] keyValuePairs = data.split("\\}\\{");

        for (String pair : keyValuePairs) {
            // Split the pair into key and value
            String[] keyValue = pair.split("(?<=\\D)(?=\\d)|(?<=\\d)(?=\\D)");

            // Extract the key and value
            int key = Integer.parseInt(keyValue[0]);
            String value = keyValue[1];

            // Add the key-value pair to the result map
            result.put(key, value);
        }

        return result;
    }

    /**
     *
     * Requirement:  System.out.println("========= TASK 2 ==========");
     * Write a method called as calculateTotalPrice1() which takes a Map of some shopping items with their amounts and calculates the total prices as double. Item prices are given below
     * Apple = $2.00
     * Orange = $3.29
     * Mango = $4.99
     * Pineapple = $5.25
     *
     */

        public static double calculateTotalPrice(Map<String, Integer> items) {
            double totalPrice = 0.0;

            for (Map.Entry<String, Integer> item : items.entrySet()) {
                String itemName = item.getKey();
                int quantity = item.getValue();

                double itemPrice = getItemPrice(itemName);
                double itemTotalPrice = itemPrice * quantity;

                totalPrice += itemTotalPrice;
            }

            return totalPrice;
        }


    private static double getItemPrice(String itemName) {
            double itemPrice;
            switch (itemName) {
                case "Apple":
                    itemPrice = 2.00;
                    break;
                case "Orange":
                    itemPrice = 3.29;
                    break;
                case "Mango":
                    itemPrice = 4.99;
                    break;
                case "Pineapple":
                    itemPrice = 5.25;
                    break;
                default:
                    throw new IllegalArgumentException("Invalid item name: " + itemName);
            }
            return itemPrice;
        }

    /** System.out.println("========= TASK 3 ==========");
     *
     * Write a method calculateTotalPrice2() which takes a Map of some shopping items with their amounts and calculates the total prices as double. Item prices are given below
     * Apple = $2.00
     * Orange = $3.29
     * Mango = $4.99
     *
     * BUT there will be some discounts as below
     * There will be %50 discount for every second Apple
     * There will be 1 free Mango if customer gets 3. So, fourth one is free.
     *
     * Test Data 1:
     * {Apple=3, Mango = 5}
     *
     * Expected Output 1:
     * 24.96
     *
     *
     */

    public static double calculateTotalPrice2(Map<String, Integer> items) {
        double totalPrice = 0.0;

        int appleCount = 0;
        int mangoCount = 0;

        for (Map.Entry<String, Integer> entry : items.entrySet()) {
            String itemName = entry.getKey();
            int quantity = entry.getValue();

            if (itemName.equals("Apple")) {
                appleCount += quantity;
                totalPrice += calculateApplePrice(quantity, appleCount);
            } else if (itemName.equals("Mango")) {
                mangoCount += quantity;
                totalPrice += calculateMangoPrice(quantity, mangoCount);
            }
        }

        return totalPrice;
    }

    private static double calculateApplePrice(int quantity, int appleCount) {
        double applePrice = 2.00;
        double discount = 0.0;

        if (appleCount % 2 == 0) {
            // Apply 50% discount for every second apple
            discount = (applePrice / 2.0) * (quantity / 2);
        }

        return (applePrice * quantity) - discount;
    }

    private static double calculateMangoPrice(int quantity, int mangoCount) {
        double mangoPrice = 4.99;
        int freeMangoCount = mangoCount / 4; // Get the number of free mangoes

        // Subtract the free mangoes from the total quantity
        quantity -= freeMangoCount;

        return mangoPrice * quantity;
    }

}





