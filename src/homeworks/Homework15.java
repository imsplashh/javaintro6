package homeworks;

import java.util.*;

public class Homework15 {
    public static void main(String[] args) {

        System.out.println("========TASK 1=========");
        System.out.println(Arrays.toString(fibonacciSeries1(3)));

        System.out.println("========TASK 2=========");
        System.out.println(fibonacciSeries2(8));

        System.out.println("========TASK 3=========");
        System.out.println(Arrays.toString(findUniques(new int[]{8, 9}, new int[]{9, 8, 9})));

        System.out.println("========TASK 4=========");
        System.out.println(firstDuplicate(new int[]{1}));

        System.out.println("========TASK 5=========");
        System.out.println(isPowerOf3(2));







    }

    /**
     *         System.out.println("========TASK 1=========");
     * Requirement:
     * -Create a method called fibonacciSeries1()
     * -This method will take an int argument as n, and it will return n series of Fibonacci numbers as an int array.
     *
     * REMEMBER: Fibonacci series = 0, 1, 1, 2, 3, 5, 8, 13, 21
     *
     */

    public static int[] fibonacciSeries1(int n) {
        int[] series = new int[n];

        if (n >= 1) {
            series[0] = 0; // First number in the series
        }

        if (n >= 2) {
            series[1] = 1; // Second number in the series
        }

        for (int i = 2; i < n; i++) {
            series[i] = series[i - 1] + series[i - 2]; // Calculate the next number in the series
        }

        return series;
    }

    /**
     * Requirement:        System.out.println("========TASK 1=========");
     * -Create a method called fibonacciSeries2()
     * -This method will take an int argument as n, and it will return the nth series of Fibonacci number as an int.
     *
     * REMEMBER: Fibonacci series = 0, 1, 1, 2, 3, 5, 8, 13, 21
     */

    public static int fibonacciSeries2(int n) {
        if (n <= 0) {
            throw new IllegalArgumentException("n must be a positive integer");
        }

        if (n == 1) {
            return 0;
        }

        int first = 0;
        int second = 1;

        for (int i = 2; i <= n; i++) {
            int next = first + second;
            first = second;
            second = next;
        }

        return first;
    }

    /**
     * System.out.println("========TASK 3=========");
     * Requirement:
     * -Create a method called findUniques()
     * -This method will take 2 int array argument and it will return an int array which has only the unique values from both given arrays.
     * NOTE: If both arrays are empty, then return an empty array.
     * NOTE: if one of the array is empty, then return unique values from the other array.
     */

    public static int[] findUniques(int[] arr1, int[] arr2) {
        HashSet<Integer> uniqueValues = new HashSet<>();

        for (int num : arr1) {
            uniqueValues.add(num);
        }

        for (int num : arr2) {
            uniqueValues.add(num);
        }

        List<Integer> uniqueList = new ArrayList<>(uniqueValues);
        int[] uniqueArray = new int[uniqueList.size()];

        for (int i = 0; i < uniqueArray.length; i++) {
            uniqueArray[i] = uniqueList.get(i);
        }

        return uniqueArray;
    }

    /**
     *
     *Requirement: System.out.println("========TASK 4=========");
     * -Create a method called firstDuplicate()
     * -This method will take an int array argument and it will return an int which is the first duplicated number.
     * NOTE: All elements will be positive numbers.
     * NOTE: If there are no duplicates, then return -1
     * NOTE: If there are more than one duplicate, then return the one for which second occurrence has the smallest index.
     *
     */

    public static int firstDuplicate(int[] arr) {
        Set<Integer> uniqueSet = new HashSet<>();

        for (int num : arr) {
            if (uniqueSet.contains(num)) {
                return num;
            }

            uniqueSet.add(num);
        }

        return -1;
    }

    /**
     * Requirement: System.out.println("========TASK 5=========");
     * -Create a method called isPowerOf3()
     * -This method will take an int argument and it will return true if given int argument is equal to 3 power of the X. Otherwise, it will return false.
     *
     * Numbers that are power of 3 = 1, 3, 9, 27, 81, 243….
     */

    public static boolean isPowerOf3(int num) {
        if (num <= 0) {
            return false;
        }

        while (num % 3 == 0) {
            num /= 3;
        }

        return num == 1;
    }

}

