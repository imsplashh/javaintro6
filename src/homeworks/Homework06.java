package homeworks;

import java.util.Arrays;
import java.util.Iterator;

public class Homework06 {
    public static void main(String[] args) {

        System.out.println("\n----------TASK-1----------");
        int[] numbers = new int[10];

        numbers[2] = 23;
        numbers[4] = 12;
        numbers[7] = 34;
        numbers[9] = 7;
        numbers[6] = 15;
        numbers[0] = 89;

        System.out.println(numbers[3]);
        System.out.println(numbers[0]);
        System.out.println(numbers[9]);
        System.out.println(Arrays.toString(numbers));

        System.out.println("\n----------TASK-2----------");

        String[] str = new String[5];

        str[1] = "abc";
        str[4] = "xyz";

        System.out.println(str[3]);
        System.out.println(str[0]);
        System.out.println(str[4]);
        System.out.println(Arrays.toString(str));

        System.out.println("\n----------TASK-3----------");

        int[] num1 = {23, -34, -56, 0, 89, 100};

        System.out.println(Arrays.toString(num1));
        Arrays.sort(num1);
        System.out.println(Arrays.toString(num1));

        System.out.println("\n----------TASK-4----------");

        String[] countries = {"Germany", "Argentina", "Ukraine", "Romania"};

        System.out.println(Arrays.toString(countries));
        Arrays.sort(countries);
        System.out.println(Arrays.toString(countries));

        System.out.println("\n----------TASK-5----------");

        String[] dogs = {"Scooby Doo", "Snoopy", "Blue", "Pluto", "Dino", "Sparky"};

        System.out.println(Arrays.toString(dogs));

        boolean hasPluto = false;

        for (String dog : dogs) {
            if(dog.equals("Pluto")) {
                hasPluto = true;
                break;
            }
        }
        System.out.println(hasPluto);

        System.out.println("\n----------TASK-6----------");

        String[] cats = {"Garfield", "Tom", "Sylvester", "Azrael"};

        Arrays.sort(cats);
        System.out.println(Arrays.toString(cats));

        boolean bothCats = false;

        for (String cat : cats) {
            if(cat.equals("Garfield") && cat.equals("Felix")){
                bothCats = true;
                break;
            }
        }
        System.out.println(bothCats);

        System.out.println("\n----------TASK-7----------");

        double[] num2 = {10.5, 20.75, 70, 80, 15.75};

        System.out.println(Arrays.toString(num2));

        for (double v : num2) {
            System.out.println(v);
        }

        System.out.println("\n----------TASK-8----------");

        char[] chars = {'A', 'b', 'G', 'H', '7', '5', '&', '*', 'e', '@', '4'};
        System.out.println(Arrays.toString(chars));

        int  countLetters = 0;
        for (char aChar : chars) {
            if(Character.isLetter(aChar)) countLetters++;
        }
        System.out.println("Letters = " + countLetters);


        int upperC = 0;
        for (char aChar : chars) {
            if(Character.isUpperCase(aChar))  upperC++;
        }
        System.out.println("Uppercase letters = " + upperC);


        int lowerC = 0;
        for (char aChar : chars) {
            if(Character.isLowerCase(aChar)) lowerC++;
        }
        System.out.println("Lowercase letters = " + lowerC);


        int digit = 0;
        for (char aChar : chars) {
            if(Character.isDigit(aChar)) digit++;
        }
        System.out.println("Digits = " + digit);


        int specialChar = 0;
        for (char aChar : chars) {
            if(!Character.isDigit(aChar) && !Character.isLetter(aChar)) specialChar++;
        }
        System.out.println("Special characters = " + specialChar);


        System.out.println("\n----------TASK-9----------");

        String[] objects = {"Pen", "notebook", "Book", "paper", "bag", "pencil", "Ruler"};

        System.out.println(Arrays.toString(objects));
        Arrays.sort(objects);

        int upperCase = 0;
        for (String object : objects) {
           upperCase++;
        }
        System.out.println("Elements starts with uppercase = " + upperCase);

        System.out.println("\n----------TASK-10----------");

        int[] num3 = {3, 5, 7, 10, 0, 20, 17, 10, 23, 56, 78};

        System.out.println(Arrays.toString(num3));

       int moreThan10 = 0;
        for (int i : num3) {
            if(i > 10) moreThan10++;
        }
        System.out.println("Elements that are more than 10 = " + moreThan10);

        int lessThan10 = 0;
        for (int i : num3) {
            if(i < 10) lessThan10++;
        }
        System.out.println("Elements that are less than 10 = " + lessThan10);

        int equal10 = 0;
        for (int i : num3) {
            if(i == 10) equal10++;
        }
        System.out.println("Elements that are 10 = " + equal10);


        System.out.println("\n----------TASK-11----------");

        int[] num4 = {5, 8, 13, 1, 2};
        int[] num5 = {9, 3, 67, 1, 0};

        System.out.println(Arrays.toString(num4));
        System.out.println(Arrays.toString(num5));











    }
}
