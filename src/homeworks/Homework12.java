package homeworks;

import java.util.Arrays;
import java.util.Collections;

public class Homework12 {
    public static void main(String[] args) {

        System.out.println("======== TASK 1 ========");
        System.out.println(noDigit("123Hello World"));

        System.out.println("======== TASK 2 ========");
        System.out.println(noVowel("TechGlobal"));

        System.out.println("======== TASK 3 ========");
        System.out.println(sumOfDigits("Johns age is 29"));

        System.out.println("======== TASK 4 ========");
        System.out.println(hasUpperCase("Java"));

        System.out.println("======== TASK 5 ========");
        System.out.println(middleInt(5, 3, 5));

        System.out.println("======== TASK 6 ========");
        System.out.println(Arrays.toString(no13(new int[]{13, 2, 3})));

        System.out.println("======== TASK 7 ========");
        System.out.println(Arrays.toString(arrFactorial(new int[]{1, 2, 3, 4})));

        System.out.println("======== TASK 8 ========");
        System.out.println(Arrays.toString(categorizeCharacters("abc123$#%")));



    }

    /*
    Requirement:
    -Create a method called noDigit()
    -This method will take one String argument and it will return a new String with
    all digits removed from the original String

    TASK 1
     */
    public static String noDigit(String str) {
        str = str.replaceAll("[0-9]", "");
        return str;
    }

    /*
    Requirement:
    -Create a method called noVowel()
    -This method will take one String argument and it will return a new String with
    all vowels removed from the original String
    TASK 2
     */
    public static String noVowel(String str) {
        str = str.replaceAll("[aeiouAEIOU]", "");
        return str;
    }

    /*
    -Create a method called sumOfDigits()
    -This method will take one String argument and it will return
    an int sum of all digits from the original String.

    TASK 3
     */
    public static int sumOfDigits(String str) {
        int sum = 0;
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            if (Character.isDigit(c)) {
                int digit = c - '0';
                sum += digit;
            }
        }
        return sum;
    }

    /*
    -Create a method called hasUpperCase()
    -This method will take one String argument and it will return boolean
    true if there is an uppercase letter and false otherwise.

    TASK 4
     */
    public static boolean hasUpperCase(String str) {
        for (int i = 0; i < str.length(); i++) {
            if (Character.isUpperCase(str.charAt(i)))
                return true;
        }
        return false;
    }

    /*
    Requirement:
    -Create a method called middleInt()
    -This method will take three int arguments and it will return the middle int.

    TASK 5
     */
    public static int middleInt(int i1, int i2, int i3) {
        Integer[] arr = {i1, i2, i3};
        Arrays.sort(arr);
        return arr[1];
    }

    /*
    Requirement:
    -Create a method called no13()
    -This method will take an int array as argument and it will return
    a new array with all 13 replaced with 0.

    TASK 6
     */
    public static int[] no13(int[] arr) {
        int[] result = new int[arr.length];
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == 13) result[i] = 0;
            else result[i] = arr[i];
        }

        return result;
    }

    /*
    Requirement:
    -Create a method called arrFactorial()
    -This method will take an int array as argument and it will return
    the array with every number replaced with their factorials.

    TASK 7
     */
    public static int[] arrFactorial(int[] arr) {
        int[] factorialArr = new int[arr.length];

        for (int i = 0; i < arr.length; i++) {
            int factorial = 1;

            for (int j = 1; j <= arr[i]; j++) {
                factorial *= j;
            }

            factorialArr[i] = factorial;
        }

        return factorialArr;
    }

    /*
    Requirement:
    -Create a method called categorizeCharacters()
    -This method will take String as an argument and return a String array as letters at index of 0, digits at index of 1 and specials at index of 2.
    NOTE: IGNORE SPACES

    Task 8
     */
    public static String[] categorizeCharacters(String str){
        String[] categories = new String[3];
        StringBuilder letters = new StringBuilder();
        StringBuilder digits = new StringBuilder();
        StringBuilder specials = new StringBuilder();

        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);

            if (Character.isLetter(c)) {
                letters.append(c);
            } else if (Character.isDigit(c)) {
                digits.append(c);
            } else if (!Character.isWhitespace(c)) {
                specials.append(c);
            }
        }

        categories[0] = letters.toString();
        categories[1] = digits.toString();
        categories[2] = specials.toString();

        return categories;

    }
}