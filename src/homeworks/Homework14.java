package homeworks;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Homework14 {
    public static void main(String[] args) {

        System.out.println("====== TASK 1 =======");
        System.out.println(fizzBuzz1(3));

        System.out.println("====== TASK 2 =======");
        System.out.println(fizzBuzz2(0));

        System.out.println("====== TASK 3 =======");
        System.out.println(findSumNumbers("a1b4c"));

        System.out.println("====== TASK 4 =======");
        System.out.println(findBiggestNumber("a1b4c 6#"));

        System.out.println("====== TASK 5 =======");
        System.out.println(countSequenceOfCharacters("abbcca"));



    }

        public static String fizzBuzz1 (int num){
            StringBuilder result = new StringBuilder();
            for (int i = 1; i <= num; i++) {
                if (i % 3 == 0 && i % 5 == 0) {
                    result.append("FizzBuzz");
                } else if (i % 3 == 0) {
                    result.append("Fizz");
                } else if (i % 5 == 0) {
                    result.append("Buzz");
                } else {
                    result.append(i);
                }
                if (i != num) {
                    result.append(" ");
                }
            }
            return result.toString();
        }
        public static String fizzBuzz2(int num) {

            String result = "";
            if (num % 3 == 0 && num % 5 == 0) {
                result = "FizzBuzz";
            } else if (num % 3 == 0) {
                result = "Fizz";
            } else if (num % 5 == 0) {
                result = "Buzz";

            }
            return result;
        }

        public static int findSumNumbers(String str){
                int sum = 0;
                String[] numbers = str.split("\\D+");

                for (String number : numbers) {
                    if (!number.isEmpty()) {
                        sum += Integer.parseInt(number);
                    }
                }

                return sum;
            }



                public static int findBiggestNumber(String str) {
                    int biggestNumber = 0;
                    Pattern pattern = Pattern.compile("\\d+");
                    Matcher matcher = pattern.matcher(str);

                    while (matcher.find()) {
                        int currentNumber = Integer.parseInt(matcher.group());
                        biggestNumber = Math.max(biggestNumber, currentNumber);
                    }

                    return biggestNumber;
                }


                    public static String countSequenceOfCharacters(String str) {
                        if (str.isEmpty()) {
                            return "";
                        }

                        StringBuilder result = new StringBuilder();
                        int count = 1;

                        for (int i = 0; i < str.length(); i++) {
                            char currentChar = str.charAt(i);

                            if (i < str.length() - 1 && str.charAt(i + 1) == currentChar) {
                                count++;
                            } else {
                                result.append(currentChar).append(count);
                                count = 1;
                            }
                        }

                        return result.toString();
                    }
}
