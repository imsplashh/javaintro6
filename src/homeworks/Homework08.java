package homeworks;

import utilities.ScannerHelper;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Homework08 {
    public static void main(String[] args) {

        System.out.println("====== task 1 =====");

        System.out.println(countConsonants("hello"));

        System.out.println("====== task 2 =====");

        System.out.println(wordArray("java is    fun"));


        System.out.println("====== task 3 =====");

        System.out.println(removeExtraSpaces("java is     fun"));

        System.out.println("====== task 4 =====");

        System.out.println(count3orLess("I go to Techglobal"));

        System.out.println("====== task 5 =====");


        System.out.println(isDateFormatValid("01/21/1999"));


        System.out.println("====== task 6 =====");


        System.out.println(isEmailFormatValid("abc@gmail.com"));

    }
    public static int countConsonants(String str){
       // System.out.println("====== task 1 =====");


        str = str.replaceAll("[aeiouAEIOU]", "");
        return str.length();

    }

    public static String wordArray(String str){
        // System.out.println("====== task 2 =====");


        String[] words = str.split("\\s+");
        return Arrays.toString(words);



    }

    public static String removeExtraSpaces(String str){
        // System.out.println("====== task 3 =====");

        String trimmed = str.trim();
        String words = trimmed.replaceAll("\\s+", " ");

        return words;



    }

    public static int count3orLess(String str){
        // System.out.println("====== task 4 =====");

        int count = 0;
        if(Pattern.matches("[A-Za-z ]", str)){
            Pattern p = Pattern.compile("[a-zA-Z ]{0,3}");
            Matcher m = p.matcher(str);
            while(m.find()){
                count++;
            }

        }
        return count;

    }


    public static boolean isDateFormatValid(String DateOfBirth){

        // System.out.println("====== task 5 =====");


        if(Pattern.matches("[0-9]{2}[/]{1}[0-9]{2}[/]{1}[0-9]{4}", DateOfBirth)) {

            return true;

        }
            else {
                return false;
        }



    }

    public static boolean isEmailFormatValid(String email){

        // System.out.println("====== task 6 =====");

        if(Pattern.matches("[a-zA-Z]{2,}[@]{1}[A-Za-z]{2,}[.]{1}[a-zA-Z]{2,}", email)){
            return true;
        } else {
            return false;
        }


    }


}
