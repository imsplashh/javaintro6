package homeworks;

import java.util.ArrayList;
import java.util.Arrays;

public class Homework13 {
    public static void main(String[] args) {

        System.out.println("--------- TASK 1 -----------");
        System.out.println(hasLowerCase("JAVA"));

        System.out.println("--------- TASK 2 -----------");
        System.out.println(noZero(new ArrayList<>(Arrays.asList(0, 1, 10))));

        System.out.println("--------- TASK 3 -----------");
        System.out.println(Arrays.deepToString(numberAndSquare(new int[]{1, 2, 3})));

        System.out.println("--------- TASK 4 -----------");
        System.out.println(containsValue(new String[]{"abc", "foo", "java"}, "hello"));

        System.out.println("--------- TASK 5 -----------");
        System.out.println(reverseSentence("Java is fun"));

        System.out.println("--------- TASK 6 -----------");
        System.out.println(removeStringSpecialsDigits("123Java #$%is fun"));

        System.out.println("--------- TASK 7 -----------");
        System.out.println(Arrays.toString(removeArraySpecialsDigits(new String[]{"123Java", "#$%is", "fun"})));

        System.out.println("--------- TASK 8 -----------");
        ArrayList<String> list1 = new ArrayList<>(Arrays.asList("Java", "is", "fun"));
        ArrayList<String> list2 = new ArrayList<>(Arrays.asList("abc", "xyz", "123"));
        System.out.println(removeAndReturnCommons(list1, list2));

        System.out.println("--------- TASK 9 -----------");
        System.out.println(noXInVariables(new ArrayList<>(Arrays.asList("xyz", "123", "#$%"))));





    }



    public static boolean hasLowerCase(String str){
        /**
         *         System.out.println("--------- TASK 1 -----------");
         * -Create a method called hasLowerCase()
         * -This method will take a String argument, and it will return boolean true if
         * there is lowercase letter and false if it doesn’t.
         */


        for (int i = 0; i < str.length(); i++) {
            if(Character.isLowerCase(str.charAt(i))) return true;
        }
        return false;
    }

    /**        System.out.println("--------- TASK 2 -----------");

     * -Create a method called noZero()
     * -This method will take one Integer ArrayList argument and it will return an ArrayList with all zeros removed from the original Integer ArrayList.
     *
     * NOTE: Assume that ArrayList size will be at least 1.
     */

    public static ArrayList<Integer> noZero(ArrayList<Integer> arr){
        ArrayList<Integer> result = new ArrayList<>();
        for (Integer num : arr) {
            if(num != 0){
                result.add(num);
            }
        }
        return result;
    }

    /**  System.out.println("--------- TASK 3 -----------");
     * Requirement:
     * -Create a method called numberAndSquare()
     * -This method will take an int array argument and it will return a multidimensional
     * array with all numbers squared.
     */

    public static int[][] numberAndSquare(int[] arr) {
        int[][] result = new int[arr.length][2];
        for (int i = 0; i < arr.length; i++) {
            result[i][0] = arr[i];
            result[i][1] = arr[i] * arr[i];
        }
        return result;
    }

    /**
     * Requirement:  System.out.println("--------- TASK 4 -----------");
     * -Create a method called containsValue()
     * -This method will take a String array and a String argument, and it will return a boolean. Search the variable inside of the array and return true if it exists in the array. If it doesn’t exist, return false.
     * NOTE: Assume that array size is at least 1.
     * NOTE: The method is case-sensitive
     */

    public static boolean containsValue(String[] arr, String str){

        for (String e : arr) {
            if(e.equals(str)){
                return true;
            }
        }

        return false;
    }

    /**           System.out.println("--------- TASK 5 -----------");

     * Requirement:
     * -Create a method called reverseSentence()
     * -This method will take a String argument and it will return a String with changing the place of every word.
     * All words should be in reverse order. Make sure that there are two words inside the sentence at least. If there is no two words return “There is not enough words!”.
     *
     * NOTE: After you reverse, only first letter must be uppercase and the rest will be lowercase!
     */

    public static String reverseSentence(String sentence) {
        String[] words = sentence.trim().split("\\s+");
        if (words.length < 2) {
            return "There is not enough words!";
        }
        StringBuilder reversed = new StringBuilder();
        for (int i = words.length - 1; i >= 0; i--) {
            String word = words[i];
            String reversedWord = word.substring(0, 1).toUpperCase() + word.substring(1).toLowerCase();
            reversed.append(reversedWord);
            if (i > 0) {
                reversed.append(" ");
            }
        }
        return reversed.toString();
    }

    /**  System.out.println("--------- TASK 6 -----------");
     * Requirement:
     * -Create a method called removeStringSpecialsDigits()
     * -This method will take a String as argument, and it will return a String without the special characters or digits.
     * NOTE: Assume that String length is at least 1.
     * NOTE: Do not remove spaces.
     */

    public static String removeStringSpecialsDigits(String str){
       String result = str.replaceAll("[^a-zA-Z\\s]", "");
        return result;
    }

    /** System.out.println("--------- TASK 7 -----------");
     * Requirement:
     * -Create a method called removeArraySpecialsDigits()
     * -This method will take a String array as argument, and it will return a String array without the special characters or digits from the elements.
     * NOTE: Assume that array size is at least 1.
     */

    public static String[] removeArraySpecialsDigits(String[] array) {
        String[] result = new String[array.length];

        for (int i = 0; i < array.length; i++) {
            String element = array[i];

            String filteredElement = element.replaceAll("[^a-zA-Z\\s]", "");
            result[i] = filteredElement;
        }

        return result;
    }

    /**  System.out.println("--------- TASK 8 -----------");
     * Requirement:
     * -Create a method called removeAndReturnCommons()
     * -This method will take two String ArrayList and it will return all the common words as String ArrayList.
     * NOTE: Assume that both ArrayList sizes are at least 1.
     */

    public static ArrayList<String> removeAndReturnCommons(ArrayList<String> list1, ArrayList<String> list2) {
        ArrayList<String> commons = new ArrayList<>();

        for (String word : list1) {
            if (list2.contains(word)) {
                commons.add(word);
            }
        }

        return commons;
    }

    /**  System.out.println("--------- TASK 9 -----------");
     * Requirement:
     * -Create a method called noXInVariables()
     * -This method will take an ArrayList argument, and it will return an ArrayList with all the x or X removed from elements.
     * If the element itself equals to x or X or contains only x letters, then remove that element.
     * NOTE: Assume that ArrayList size is at least 1
     */

    public static ArrayList<String> noXInVariables(ArrayList<String> list) {
        ArrayList<String> result = new ArrayList<>();

        for (String element : list) {
            if (!element.equalsIgnoreCase("x") && !element.equalsIgnoreCase("xx")) {
                result.add(element.replaceAll("[xX]", ""));
            }
        }

        return result;
    }

}
