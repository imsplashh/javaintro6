package homeworks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;

public class Homework10 {
    public static void main(String[] args) {

        System.out.println("-------- task 1 --------");

        System.out.println(countWords("Java is fun"));

        System.out.println("-------- task 2 --------");

        System.out.println(countA("QA stands for Quality Assurance"));

        System.out.println("-------- task 3 --------");

        System.out.println(countPos(new ArrayList<>(Arrays.asList(-45, 0, 0, 34, 5, 67))));

        System.out.println("-------- task 4 --------");

        System.out.println(removeDuplicateNumbers(new ArrayList<>(Arrays.asList(10, 20, 35, 20, 35, 60, 70, 60))));

        System.out.println("-------- task 5 --------");

        System.out.println(removeDuplicateElements(new ArrayList<>(Arrays.asList("java", "C#", "ruby", "JAVA", "ruby", "C#", "C++"))));

        System.out.println("-------- task 6 --------");

        System.out.println(removeExtraSpaces("   I   am      learning     Java      "));

        System.out.println("-------- task 7 --------");

        System.out.println(Arrays.toString(add(new int[]{3, 0, 0, 7, 5, 10}, new int[]{6, 3, 2})));


        System.out.println("-------- task 8 --------");

        System.out.println(findClosestTo10Excluding10(new int[]{10, -13, 5, 70, 15, 57}));


    }

    /*
            System.out.println("-------- task 1 --------");
            Write a method countWords() that takes a String as an argument,
            and returns how many words there are in the the given String

     */
    public static int countWords(String str) {

        String[] words = str.split(" ");

        return words.length;

    }
    /*
                System.out.println("-------- task 2 --------");

    Write a method countA() that takes a String as an argument,
    and returns how many A or a there are in the the given String

     */

    public static int countA(String str) {
        int count = 0;
        for (int i = 0; i < str.length() - 1; i++) {
            if (str.charAt(i) == 'a' || str.charAt(i) == 'A') count++;
        }
        return count;
    }

    /*
                System.out.println("-------- task 3 --------");

        Write a method countPos() that takes an ArrayList of Integer as an argument,
        and returns how many elements are positive

     */
    public static int countPos(ArrayList<Integer> arr) {
        int count = 0;

        for (int num : arr) {
            if (num > 0) count++;
        }
        return count;
    }

    /*

                    System.out.println("-------- task 4 --------");

            Write a method removeDuplicateNumbers() that takes an ArrayList of Integer as an argument,
            and returns it back with removed duplicates

     */

    public static ArrayList<Integer> removeDuplicateNumbers(ArrayList<Integer> arr) {
        Collections.sort(arr);
        HashSet<Integer> set = new HashSet<>();

        for (Integer integer : arr) {
            set.add(integer);


        }
        return new ArrayList<>(set);
    }

    /*
                        System.out.println("-------- task 5 --------");
                        Write a method removeDuplicateElements() that takes an ArrayList of String as an argument,
                        and returns it back with removed duplicates
     */
    public static ArrayList<String> removeDuplicateElements(ArrayList<String> arr) {
        HashSet<String> nonDuplicates = new HashSet<>();
        for (String s : arr) {
            nonDuplicates.add(s);
        }

        return new ArrayList<>(nonDuplicates);

    }

    /*
                            System.out.println("-------- task 6 --------");
                            Write a method removeExtraSpaces() that takes a String as an argument,
                            and returns a String with removed extra spaces
     */

    public static String removeExtraSpaces(String str) {
        String[] words = str.trim().split("\\s+");

        StringBuilder sb = new StringBuilder();
        for (String word : words) {
            sb.append(word).append(" ");
        }
        return sb.toString().trim();
    }

    /*
                                System.out.println("-------- task 7 --------");
                                Write a method add() that takes 2 int[] arrays as arguments
                                and returns a new array with sum of given arrays elements.
     */

    public static int[] add(int[] arr1, int[] arr2) {
        int len = Math.max(arr1.length, arr2.length);
        int[] result = new int[len];
        for (int i = 0; i < len; i++) {
            int val1 = i < arr1.length ? arr1[i] : 0;
            int val2 = i < arr2.length ? arr2[i] : 0;
            result[i] = val1 + val2;
        }
        return result;
    }

    /*
                                    System.out.println("-------- task 8 --------");
                                    Write a method findClosestTo10() that takes an int[] array as an argument,
                                    and returns the closest element to 10 from given array
     */

    public static int findClosestTo10Excluding10(int[] arr) {
        int closest = Integer.MAX_VALUE;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] != 10 && Math.abs(10 - arr[i]) < Math.abs(10 - closest)) {
                closest = arr[i];
            }
        }
        return closest;
    }

}