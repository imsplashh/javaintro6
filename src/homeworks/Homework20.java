package homeworks;

public class Homework20 {
    public static void main(String[] args) {

        System.out.println(" ---------- TASK 1 -----------");
        System.out.println(sum(new int[]{1, 5, 10}, true));

        System.out.println(" ---------- TASK 2 -----------");
        System.out.println(sumDigitsDouble("ab12"));


        System.out.println(" ---------- TASK 3 -----------");
        System.out.println(countOccurrence("Hello", "l"));






    }

    /**
     * System.out.println(" ---------- TASK 1 -----------");
     * Requirement:
     * Write a method called as sum() that takes an int array and a boolean  and returns
     * the either the count of even or odd numbers based on boolean value.
     * <p>
     * NOTE: if the boolean value is true, the method should return the count of the even elements.
     * If the boolean value is false, the method should return the count of the odd elements.
     */
    public static int sum(int[] arr, boolean countEven) {
        int count = 0;
        for (int num : arr) {
            if (countEven && num % 2 == 0) { // Count even numbers
                count++;
            } else if (!countEven && num % 2 != 0) { // Count odd numbers
                count++;
            }
        }
        return count;

    }


    /**
     * System.out.println(" ---------- TASK 2 -----------");
     * Requirement:
     * Write a method called as sumDigitsDouble() that takes a String and returns the sum of the digits in
     * the given String multiplied by.
     * <p>
     * NOTE: your method should return -1 if the given String does not have any digits. Ignore negative numbers.
     */
    public static double sumDigitsDouble(String input) {
        int sum = 0;
        boolean hasDigits = false;

        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            if (Character.isDigit(c)) {
                int digit = Character.getNumericValue(c);
                if (digit >= 0) {
                    sum += digit;
                    hasDigits = true;
                }
            }
        }

        if (hasDigits) {
            return sum * 2.0;
        } else {
            return -1;
        }
    }

    public static int countOccurrence(String first, String second) {
        first = first.replaceAll("\\s", "").toLowerCase();
        second = second.replaceAll("\\s", "").toLowerCase();

        int count = 0;
        int index = 0;
        while ((index = second.indexOf(first, index)) != -1) {
            count++;
            index += first.length();
        }

        return count;
    }

}
