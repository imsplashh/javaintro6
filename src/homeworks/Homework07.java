package homeworks;

import com.sun.org.apache.xalan.internal.xsltc.dom.KeyIndex;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;

public class Homework07 {
    public static void main(String[] args) {

        System.out.println("\n-----TASK 1 ------");
        ArrayList<Integer> numbers = new ArrayList<>(Arrays.asList(10, 23, 67, 23, 78));

        System.out.println(numbers.get(3));
        System.out.println(numbers.get(0));
        System.out.println(numbers.get(2));
        System.out.println(numbers);

        System.out.println("\n-----TASK 2 ------");

        ArrayList<String> colors = new ArrayList<>(Arrays.asList("Blue", "Brown", "Red", "White", "Black", "Purple"));

        System.out.println(colors.get(1));
        System.out.println(colors.get(3));
        System.out.println(colors.get(5));
        System.out.println(colors);

        System.out.println("\n-----TASK 3 ------");

        ArrayList<Integer> numbers2 = new ArrayList<>(Arrays.asList(23, -34, -56, 0, 89, 100));

        System.out.println(numbers2);

        Collections.sort(numbers2);

        System.out.println(numbers2);

        System.out.println("\n-----TASK 4 ------");

        ArrayList<String> cities = new ArrayList<>(Arrays.asList("Istanbul", "Berlin", "Madrid", "Paris"));

        System.out.println(cities);

        Collections.sort(cities);

        System.out.println(cities);

        System.out.println("\n-----TASK 5 ------");

        ArrayList<String> marvelCharacters = new ArrayList<>(Arrays.asList("Spider Man", "Iron Man", "Black Panter", "Deadpool", "Captain America"));

        System.out.println(marvelCharacters);

        for (String marvelCharacter : marvelCharacters) {
            if(marvelCharacter.equalsIgnoreCase("wolverine")) System.out.println("true");
            else System.out.println("false");
            break;

        }

        System.out.println("\n-----TASK 6 ------");

        ArrayList<String> avengersCharacters = new ArrayList<>(Arrays.asList("Hulk", "Black Widow", "Captain America", "Iron Man"));
        Collections.sort(avengersCharacters);
        System.out.println(avengersCharacters);

        for (String avengersCharacter : avengersCharacters) {
            if(avengersCharacter.contains("Hulk") && avengersCharacter.contains("Iron Man")) System.out.println("false");
            else System.out.println("true");
            break;

        }

        System.out.println("\n-----TASK 7 ------");

        ArrayList<Character> characters = new ArrayList<>(Arrays.asList('A', 'x', '$', '%', '9', '*', '+', 'F', 'G'));

        System.out.println(characters);

        for (Character character : characters) {
            System.out.println(character);
        }

        System.out.println("\n-----TASK 8 ------");

        ArrayList<String> objects = new ArrayList<>(Arrays.asList("Desk", "Laptop", "Mouse", "Monitor", "Mouse-Pad", "Adapter"));

        System.out.println(objects);
        Collections.sort(objects);
        System.out.println(objects);

        int countM = 0;
        int countAorE = 0;

        for (String object : objects) {
            if(object.startsWith("M") || object.startsWith("m")) countM++;
           else if(!object.contains("A") || !object.contains("a") || !object.contains("E")
            || !object.contains("e")) countAorE++;
        }
        System.out.println(countM);
        System.out.println(countAorE);

        System.out.println("\n-----TASK 9 ------");

        ArrayList<String> kitchenObjects = new ArrayList<>(Arrays.asList("Plate", "spoon", "fork", "Knife", "cup", "Mixer"));

        int hasP = 0;
        int startsOrEndsWithP = 0;

        System.out.println(kitchenObjects);

        for (String kitchenObject : kitchenObjects) {
            if(kitchenObject.contains("p") || kitchenObject.contains("P")) hasP++;
        }
        System.out.println(hasP);

        for (String kitchenObject : kitchenObjects) {
            if(kitchenObject.startsWith("P") || kitchenObject.startsWith("p") || kitchenObject.endsWith("p") || kitchenObject.endsWith("P")) startsOrEndsWithP++;
        }

        System.out.println(startsOrEndsWithP);


        System.out.println("\n-----TASK 10 ------");

        ArrayList<Integer> numbers3 = new ArrayList<>(Arrays.asList(3, 5, 7, 10, 0, 20, 17, 10, 23, 56, 78));

        int numbersDividedBy10 = 0;
        int numbersGreater15 = 0;
        int numbersLess20 = 0;
        int numbersLess15rGreater50 = 0;

        System.out.println(numbers3);

        for (Integer integer : numbers3) {
            if(integer % 10 == 0) numbersDividedBy10++;
            else if(integer % 2 == 0 && integer > 15) numbersGreater15++;
            else if(integer % 2 == 1 && integer < 20)  numbersLess20++;
            else if(integer < 15 || integer > 50) numbersLess15rGreater50++;
        }

        System.out.println(numbersDividedBy10);
        System.out.println(numbersGreater15);
        System.out.println(numbersLess20);
        System.out.println(numbersLess15rGreater50);

    }
}
