package homeworks;

import java.sql.SQLOutput;
import java.util.Random;
import java.util.Scanner;

public class Homework03 {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
/*
        System.out.println("\n----------TASK-1----------");

        System.out.println("Please enter 2 numbers");
        int num1 = input.nextInt();
        int num2 = input.nextInt();

        System.out.println("The difference between numbers is = " + Math.abs(num1 - num2));



        System.out.println("\n----------TASK-2----------");

        int number1, number2, number3, number4, number5;

        System.out.println("Please enter 5 numbers");
        number1 = input.nextInt();
        number2 = input.nextInt();
        number3 = input.nextInt();
        number4 = input.nextInt();
        number5 = input.nextInt();


        System.out.println("Max value = " + Math.max(Math.max(Math.max(number1, number2), Math.max(number3, number4)), number5));
        System.out.println("Min value = " + Math.min(Math.min(Math.min(number1, number2), Math.min(number3, number4)), number5));



        System.out.println("\n----------TASK-3----------");


        int random1 = (int) (Math.random () * 51 + 50);
        int random2 = (int) (Math.random () * 51 + 50);
        int random3 = (int) (Math.random () * 51 + 50);

        System.out.println("Number 1 = " + random1);
        System.out.println("Number 2 = " + random2);
        System.out.println("Number 3 = " + random3);

        int result = random1 + random2 + random3;

        System.out.println("The sum of the numbers is = " + result);



        System.out.println("\n----------TASK-4----------");

        double theMoneyOfAlex =125;
        double theMoneyOfMike =220;
        double theMoneyOfAlex1 = theMoneyOfAlex - 25.5;
        double theMoneyOfMike1 = theMoneyOfMike + 25.5;

        System.out.println("Alex's money: $" +theMoneyOfAlex1);

        System.out.println("Mike's money: $" + theMoneyOfMike1);




        System.out.println("\n----------TASK-5----------");

        double price = 390;
        double savingPerDay =15.60;

        int day= (int) (price/savingPerDay);

        System.out.println(day);



        System.out.println("\n----------TASK-6----------");


        String s1 = "5", s2 = "10";
        int a1 = Integer.parseInt(s1);
        int a2 = Integer.parseInt(s2);

        System.out.println("Sum of 5 and 10 is = " + (a1 + a2));

        System.out.println("Product of 5 and 10 is = " + a1 * a2);

        System.out.println("Division of 5 and 10 is = " + a1 / a2);

        System.out.println("Subtraction of 5 and 10 is = " + (a1 - a2));

        System.out.println("Remainder of 5 and 10 is = " + a1 % a2);



        System.out.println("\n----------TASK-7----------");


        String b1 = "200", b2 = "-50";

        int B1 = Integer.parseInt(b1);
        int B2 = Integer.parseInt(b2);

        int average = (B1 + B2) / 2;

        System.out.println("The greatest value is = " + Integer.max(B1, B2));
        System.out.println("The smallest value is = " + Integer.min(B1, B2));
        System.out.println("The average is = " + average);
        System.out.println("The absolute difference is = " + Math.abs(B2- B1));


        System.out.println("\n----------TASK-8----------");


        double dailySaveAmount = .96;
        double price1 = 24;
        double price2 = 168;
        double month = 5;
        double days = 30;

        int totalDays = (int) (days*month);
        int totalAfter5Months = (int) (totalDays*dailySaveAmount);
        int numberOfDays1 = (int) (price1/dailySaveAmount);
        int numberOfDays2 = (int) (price2/dailySaveAmount);

        System.out.println(numberOfDays1 +" days");
        System.out.println(numberOfDays2 +" days");
        System.out.println("$" +(double) + (totalAfter5Months));



        System.out.println("\n----------TASK-9----------");

        double totalToSave = 1250;
        double perDaySaving = 62.5;

        int DaysForComputer = (int) (totalToSave/perDaySaving);

        System.out.println( (int) DaysForComputer);



        System.out.println("\n----------TASK-10----------");


        double TotalToSave = 14265;
        double option1 = 475.50;
        double option2 = 951;

        int DaysForOption1 = (int) (TotalToSave/option1);
        int DaysForOption2 = (int) (TotalToSave/option2);

        System.out.println("Option 1 will take " +DaysForOption1 + " months");
        System.out.println("Option 2 will take " +DaysForOption2 + " months");



        System.out.println("\n----------TASK-11----------");


        int number1 = input.nextInt();
        int number2 = input.nextInt();

        double firstNumber = number1;
        double secondNumber = number2;

        System.out.println(firstNumber/secondNumber);



        System.out.println("\n----------TASK-12----------");


        int d1= (int) (Math.random() * 101);
        int d2= (int) (Math.random() * 101);
        int d3= (int) (Math.random() * 101);

        System.out.println(d1);
        System.out.println(d2);
        System.out.println(d3);


        if(d1>25 && d2>25 && d3>25){
            System.out.println("True");

        }
        else{
            System.out.println("False");
        }



        System.out.println("\n----------TASK-13----------");

        Random r = new Random();

        int daysOfWeek = r.nextInt(7) + 1;
        System.out.println(daysOfWeek);

        if(daysOfWeek == 1) System.out.println("Sunday");
        else if(daysOfWeek == 2) System.out.println("Monday");
        else if(daysOfWeek == 3) System.out.println("Tuesday");
        else if(daysOfWeek == 4) System.out.println("Wednesday");
        else if(daysOfWeek == 5) System.out.println("Thursday");
        else if(daysOfWeek == 6) System.out.println("Friday");
        else System.out.println("Saturday");




        System.out.println("\n----------TASK-14----------");

        System.out.println("Tell me your exam results?");

        int exam1 = input.nextInt();
        int exam2 = input.nextInt();
        int exam3 = input.nextInt();

        int average = (exam1 + exam2 + exam3) / 3;



        if(average >= 70 ){
            System.out.println("YOU PASSED!");
        }
        else{
            System.out.println("YOU FAILED!");
        }



*/

        System.out.println("\n----------TASK-15----------");


        System.out.println("Enter 3 numbers");

        int userNumber1 = input.nextInt();
        int userNumber2 = input.nextInt();
        int userNumber3 = input.nextInt();

        String allNumbers = "" + userNumber1 + userNumber2 + userNumber3;


        if(userNumber1 == userNumber2 && userNumber1 == userNumber3){
            System.out.println("TRIPLE MATCH");
        }
        else if(userNumber1 == userNumber2 || userNumber2 == userNumber3 || userNumber3 == userNumber1){
            System.out.println("DOUBLE MATCH");
        }
        else{
            System.out.println("NO MATCH");
        }




    }
}
