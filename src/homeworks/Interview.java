package homeworks;

import java.util.*;

public class Interview {
    public static void main(String[] args) {

        /*
        Requirement:
        -Write a public static method that takes an int array as an argument and returns an int array.
        -The method should add 3 to each element and return it back.
        -Method name can be called as add3.

        Test data:
        [2, 5, 0, 4]

        Expected:[5, 8, 3, 7]
         */
        System.out.println(Arrays.toString(add3(new int[]{2, 5, 0, 4})));

        System.out.println(findMax(new ArrayList<>(Arrays.asList(-20, 35, 70, 4, 5))));

        System.out.println(countUniqueElements(new String[] {"Apple", "Apple", "Orange", "Apple", "Kiwi"}));




    }

    public static int[] add3(int[] arr){
        int[] result = new int[4];
        for (int i = 0; i < arr.length; i++) {
            result[i] = arr[i] + 3;
        }
        return result;
        }



         /*
        Requirement:
        -Write a public static method that takes an Integer ArrayList as an argument and returns an int.
        -The method should find the max value from the list and return it.
        -Method name can be called as findMax.
        NOTE: DO NOT use sort method.

        Test data:
        [-20, 35, 70, 4, 5]

        Expected:70
         */

    public static int findMax(ArrayList<Integer> arr){
        //Collections.sort(arr);
        //return arr.get(4);
       TreeSet<Integer> set = new TreeSet<>(arr);
       return set.last();


    }

        public static Map<String, Integer> countUniqueElements(String[] array) {
            Map<String, Integer> elementCount = new HashMap<>();

            for (String element : array) {
                // Get the current count of the element, or 0 if it doesn't exist in the map yet
                int count = elementCount.getOrDefault(element, 0);

                // Increment the count and put it back in the map
                elementCount.put(element, count + 1);
            }

            return elementCount;
        }


    }
