package homeworks;

import java.util.Arrays;

public class Homework18 {
    public static void main(String[] args) {
        System.out.println("======= TASK 1 ======");
        System.out.println(Arrays.toString(doubleOrTriple(new int[]{5, 7, 2}, false)));

        System.out.println("======= TASK 2 ======");
        System.out.println(splitString("Java", 2));

        System.out.println("======= TASK 3 ======");
        System.out.println(countPalindrome("Mom and dad"));


    }

    /**         System.out.println("======= TASK 1 ======");
     * Requirement:
     * Write a method called as doubleOrTriple() that takes an int array and a boolean  and
     * returns the array elements either doubled or tripled based on boolean value.
     * NOTE: if the boolean value is true, the elements in the array should be doubled.
     * If the boolean value is false, the elements should be tripled.
     */

    public static int[] doubleOrTriple(int[] arr, boolean doubleElements){
        int[] result = new int[arr.length];

        for (int i = 0; i < arr.length; i++) {
            if(doubleElements){
                result[i] = arr[i] * 2;
            } else {
                result[i] = arr[i] * 3;
            }
        }
         return result;
    }

    /**      System.out.println("======= TASK 2 ======");
     * Requirement:
     * Write a method called as splitString() that takes a String and an int arguments and returns the String back split by the given int.
     *
     * NOTE: your method should return empty String if the length of given String cannot be divided to given number.
     *
     * Test Data 1:
     * (”Java", 2)
     *
     * Expected Output 1:
     * “Ja va”
     */

    public static String splitString(String str, int splitSize) {
        // Check if the length of the string can be divided evenly by splitSize
        if (str.length() % splitSize != 0) {
            return ""; // Return empty string if the length cannot be divided evenly
        }

        StringBuilder result = new StringBuilder();

        // Iterate over the string and split it into substrings of splitSize length
        for (int i = 0; i < str.length(); i += splitSize) {
            String substring = str.substring(i, i + splitSize);
            result.append(substring).append(" ");
        }

        return result.toString().trim(); // Trim trailing space before returning the result
    }


    /**  System.out.println("======= TASK 3 ======");
     *  Requirement:
     * Write a method countPalindrome() that takes String and returns how many words in the String are palindrome words.
     * NOTE: A palindrome word is a word that reads the same forwards and backwards. Example: level, radar, deed, refer…
     * NOTE: This method is case-insensitive!
     *
     * Test Data 1:
     * “Mom and Dad”
     *
     * Expected Output 1:
     * 2
     */

    public static int countPalindrome(String str) {
        String[] words = str.split("\\s+");
        int count = 0;

        for (String word : words) {
            String cleanedWord = word.replaceAll("[^a-zA-Z0-9]", "").toLowerCase();
            boolean isPalindrome = true;

            for (int i = 0, j = cleanedWord.length() - 1; i < j; i++, j--) {
                if (cleanedWord.charAt(i) != cleanedWord.charAt(j)) {
                    isPalindrome = false;
                    break;
                }
            }

            if (isPalindrome) {
                count++;
            }
        }

        return count;
    }
}
