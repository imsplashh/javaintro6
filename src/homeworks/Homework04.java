package homeworks;

import utilities.ScannerHelper;

import java.util.Scanner;

public class Homework04 {
    public static void main(String[] args) {

        /*
        System.out.println("\n----------TASK-1----------");


        String name = ScannerHelper.getFirstName();
        int L = name.length();

        char A = name.charAt(0);

        System.out.println("The length of the name = " + name.length());

        System.out.println("The first character in the name = " + name.charAt(0));

        System.out.println("The last character in the name = " + name.charAt(L - 1));

        System.out.println("The first 3 characters in the name = " + name.substring(0,3) );

        System.out.println("The last 3 characters in the name = " + name.substring(L  - 3));

        if(A == 'A'|| A == 'a'){
            System.out.println("You are in the club!");
            }
        else{
            System.out.println("Sorry, you are not in the club");
        }



        System.out.println("\n----------TASK-2----------");


        System.out.println("Please enter your full address");

        Scanner input = new Scanner(System.in);

        String fullAddress = input.nextLine();

        boolean city1 = fullAddress.contains("Chicago");
        boolean city2 = fullAddress.contains("chicago");
        boolean city3 = fullAddress.contains("Des Plaines");
        boolean city4 = fullAddress.contains("des plaines");

        if(city1 || city2){
            System.out.println("You are in the club");
        }
       else if(city3 || city4){
            System.out.println("You are welcome to join to the club");
        }
        else System.out.println("Sorry, you will never be in the club");


        System.out.println("\n----------TASK-3----------");

        String favCountry = ScannerHelper.getFavCountry();

        boolean a = favCountry.toLowerCase().contains("a");

        boolean i = favCountry.toLowerCase().contains("i");

        if(favCountry.toLowerCase().contains("a") || favCountry.toLowerCase().contains("i")){
            System.out.println("A and i are there");
        }
        else if(!favCountry.toLowerCase().contains("a") || !favCountry.toLowerCase().contains("i")){
            System.out.println("A and i are not there");
        }
       else if (favCountry.toLowerCase().contains("i")){
            System.out.println("I is there");

        }
        else if(favCountry.toLowerCase().contains("a")){
            System.out.println("A is there");
        }

*/

        System.out.println("\n----------TASK-4----------");


        String str = "   Java is FUN   ";

        System.out.println("The first word in str is = " + str.substring(3,7));

        System.out.println("The second word in str is = " + str.substring(8,10));

        System.out.println("The third word in str is = " + str.substring(11));





    }
}
