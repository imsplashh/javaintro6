package homeworks;

import java.util.Arrays;

public class Homework19 {
    public static void main(String[] args) {


        System.out.println("========= TASK 1 =========");
        System.out.println(sum(new int[]{1, 5, 10}, true));

        System.out.println("========= TASK 2 =========");

        System.out.println(nthChars("java", 3));

        System.out.println("========= TASK 3 =========");

        System.out.println(canFormString("Hello", "Hi"));

        System.out.println("========= TASK 4 =========");
        System.out.println(isAnagram("apple", "peach"));








    }

    /**
     * * Requirement:
     *      * Write a method called as sum() that takes an int array and a boolean  and returns the sum
     *      * of the numbers positioned at even-odd indexes based on boolean value.
     *      *
     *      * NOTE: if the boolean value is true, the method should return the sum of the elements that
     *      * have even indexes. If the boolean value is false, the method should return the sum of the elements that
     *      * have odd indexes.
     *
     */

    public static int sum(int[] arr, boolean evenIndex) {
        int sum = 0;
        for (int i = 0; i < arr.length; i++) {
            if ((i % 2 == 0 && evenIndex) || (i % 2 != 0 && !evenIndex)) {
                sum += arr[i];
            }
        }
        return sum;
    }

    /**Requirement:
     Write a method called as nthChars() that takes a String and an int arguments
     and returns the String back with every nth characters.

     NOTE: your method should return empty String if the length of given String is less than the given number.

     */
     public static String nthChars(String input, int n) {
        if (input.length() < n) {
            return "";
        }

        StringBuilder result = new StringBuilder();
        for (int i = 0; i < input.length(); i += n) {
            result.append(input.charAt(i));
        }

        return result.toString();
    }

    public static boolean canFormString(String str1, String str2) {
        // Remove white spaces and convert both strings to lowercase
        String formattedStr1 = str1.replaceAll("\\s", "").toLowerCase();
        String formattedStr2 = str2.replaceAll("\\s", "").toLowerCase();


        char[] charArray1 = formattedStr1.toCharArray();
        char[] charArray2 = formattedStr2.toCharArray();
        Arrays.sort(charArray1);
        Arrays.sort(charArray2);


        return Arrays.equals(charArray1, charArray2);
    }

    public static boolean isAnagram(String str1, String str2) {
        // Remove whitespace and convert to lowercase
        String s1 = str1.replaceAll("\\s", "").toLowerCase();
        String s2 = str2.replaceAll("\\s", "").toLowerCase();

        // Sort the characters of both strings
        char[] arr1 = s1.toCharArray();
        char[] arr2 = s2.toCharArray();
        Arrays.sort(arr1);
        Arrays.sort(arr2);

        // Compare the sorted strings
        return Arrays.equals(arr1, arr2);
    }
}
