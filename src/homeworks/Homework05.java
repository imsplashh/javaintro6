package homeworks;

import utilities.ScannerHelper;

public class Homework05 {
    public static void main(String[] args) {

        System.out.println("\n----------TASK-1----------");

        for (int i = 1; i <= 100; i++) {
            if(i % 7 ==0) System.out.print(i + "-");

        }

        System.out.println("\n----------TASK-2----------");

        for (int i = 1; i <= 50; i++) {
            if(i % 2 == 0 && i % 3 == 0) System.out.print(i + "-");

        }

        System.out.println("\n----------TASK-3----------");

        for (int i = 100; i >=  50; i--) {
            if(i % 5 == 0) System.out.print(i + "-");
        }

        System.out.println("\n----------TASK-4----------");


        for (int i = 0; i <= 7; i++) {
          System.out.println("The square of " + i + " is = " + (i * i));

        }


        System.out.println("\n----------TASK-5----------");


        int sum = 0;

        for (int i = 1; i <= 10; i++) {
            sum += i;
        }
        System.out.println(sum);


        System.out.println("\n----------TASK-6----------");


        int number = ScannerHelper.getNumber();

        int factorial = 1;

        for (int i = 1; i <= number; i++) {
           factorial *= i;

        }

        System.out.println(factorial);


        System.out.println("\n----------TASK-7----------");

        String fullName = ScannerHelper.getString();

        int vowels = 0;

        for (int i = 0; i <= fullName.length()-1 ; i++) {
            if(fullName.toLowerCase().charAt(i) == 'a' || fullName.toLowerCase().charAt(i) == 'e' ||
            fullName.toLowerCase().charAt(i) == 'i' || fullName.toLowerCase().charAt(i) == 'o' ||
                    fullName.toLowerCase().charAt(i) == 'u') vowels++;
        }
        System.out.println(vowels);



        System.out.println("\n----------TASK-8----------");


       String name;

        do{
            name = ScannerHelper.getFirstName();
        }
        while(name.charAt(0) != 'J' && name.charAt(0) != 'j');

        System.out.println("End of the program");

        }


    }

