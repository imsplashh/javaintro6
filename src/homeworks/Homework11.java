package homeworks;

import java.time.LocalDate;
import java.util.Arrays;

public class Homework11 {
    public static void main(String[] args) {

        System.out.println("------- Task 1 ----------");

        System.out.println(noSpace("Hello world"));

        System.out.println("------- Task 2 ----------");
        System.out.println(replaceFirstLast("Hello"));

        System.out.println("------- Task 3 ----------");
        System.out.println(hasVowel("Java"));

        System.out.println("------- Task 4 ----------");
        System.out.println(checkAge(2006));

        System.out.println("------- Task 5 ----------");
        System.out.println(averageOfEdges(0, 0, 6));

        System.out.println("------- Task 6 ----------");
        System.out.println(Arrays.toString(noA(new String[]{"java", "hello", "ABC", "xyz"})));

        System.out.println("------- Task 7 ----------");
        System.out.println(Arrays.toString(no3or5(new int[]{7, 4, 11, 23, 17})));

        System.out.println("------- Task 8 ----------");
        System.out.println(countPrimes(new int[]{-10, -3, 0, 1}));







    }

    /*
            System.out.println("------- Task 1 ----------");
            -Create a method called noSpace()
            -This method will take one String argument and it will return
            a new String with all spaces removed from the original String
     */

    public static String noSpace(String str) {
        return str.replaceAll(" ", "");
    }

    /*
            System.out.println("------- Task 2 ----------");
            -Create a method called replaceFirstLast()
            -This method will take one String argument and it will return
            a new String with first and last characters replaced

            NOTE: if the length is less than 2, then return empty String
            NOTE: Ignore all before and after spaces (get actual String only)


     */

    public static String replaceFirstLast(String str) {
        str = str.trim(); // ignores any leading or trailing white spaces

        if (str.length() < 2) return "";
        else return str.charAt(str.length() - 1) + str.substring(1, str.length() - 1) + str.charAt(0);

    }

    /*
            System.out.println("------- Task 3 ----------");
            -Create a method called hasVowel()
            -This method will take one String argument and it will return a boolean
             checking if String has any vowel or not


     */

    public static boolean hasVowel(String str){

       return str.matches(".*[aeiouAEIOU].*");
    }

    /*
                System.out.println("------- Task 4 ----------");
                -Create a method called checkAge()
                -This method will take an int yearOfBirth as  argument and it will print message
                below based on the entry
                If the age is less than 16, then print “AGE IS NOT ALLOWED”
                If the age is 16 or more, then print “AGE IS ALLOWED”
                If the age is more than 100 or a future year entered, print “AGE IS NOT VALID”
                NOTE: Calculate the age taking base year as current year in a dynamic way.
                You can use Date class.
     */

    public static String checkAge(int yearOfBirth) {
        LocalDate now = LocalDate.now();
        int currentYear = now.getYear();
        int age = currentYear - yearOfBirth;

        if (age < 0 || age > 100) {
            return "AGE IS NOT VALID";
        } else if (age < 16) {
            return "AGE IS NOT ALLOWED";
        } else {
            return "AGE IS ALLOWED";
        }
    }

    /*
            System.out.println("------- Task 5 ----------");
            -Create a method called averageOfEdges()
            -This method will take three int arguments and it will return
            average of min and max numbers
     */

    public static int averageOfEdges(int a, int b, int c){
         int max = Math.max(Math.max(a, b), c);
         int min = Math.min(Math.min(a, b), c);
         return (max + min) / 2;
    }

    /*
                System.out.println("------- Task 6 ----------");

    -Create a method called noA()
     -This method will take a String array argument and it will return a new array
      with all elements starting with A or a replaced with “###”
     */

    public static String[] noA(String[] arr){
        String[] result = new String[arr.length];
        for (int i = 0; i < arr.length; i++) {
            if (arr[i].startsWith("A") || arr[i].startsWith("a")) {
                result[i] = "###";
            } else {
                result[i] = arr[i];
            }
        }
        return result;
    }

    /*
                System.out.println("------- Task 7 ----------");

    -Create a method called no3or5()
    -This method will take an int array argument and it will return a new array with some elements replaced as below
    If element can be divided by 5, replace it with 99
    If element can be divided by 3, replace it with 100
    If element can be divided by both 3 and 5, replace it with 101

    NOTE: Assume length will always be more than zero
     */

    public static int[] no3or5(int[] arr){
        int[] result = new int[arr.length];

        for (int i = 0; i < arr.length; i++) {
            if(arr[i] % 5 == 0 && arr[i] % 3 == 0) result[i] = 101;
            else if(arr[i] % 3 == 0) result[i] = 100;
            else if(arr[i] % 5 == 0) result[i] = 99;
            else  result[i] = arr[i];
        }
        return result;
    }

    /*
                    System.out.println("------- Task 8 ----------");

    -Create a method called countPrimes()
    -This method will take an int array argument and it will return how many elements in the array are prime numbers

    NOTE: Prime number is a number that can be divided only by 1 and itself
    NOTE: Negative numbers cannot be prime
    Examples: 2,3,5,7,11,13,17,19,23,29,31,37 etc.
    NOTE: Smallest prime number is 2

     */

    public static int countPrimes(int[] arr) {
        int count = 0;
        for (int num : arr) {
            if (num <= 1) continue; // skip negative numbers and 0 or 1
            boolean isPrime = true;
            for (int i = 2; i <= Math.sqrt(num); i++) {
                if (num % i == 0) {
                    isPrime = false;
                    break;
                }
            }
            if (isPrime) count++;
        }
        return count;
    }


}
