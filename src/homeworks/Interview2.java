package homeworks;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

public class Interview2 {
    public static void main(String[] args) {

        System.out.println(countElement(new String[]{"Apple", "Apple", "Orange", "Apple", "Kiwi"}));


    }

    public static Map<String, Integer> countElement(String[] array){
        Map<String, Integer> result = new LinkedHashMap<>();

        for (String s : array) {
            int count = result.getOrDefault(s, 0);

            result.put(s, count + 1);
        }
        return result;
    }

}
