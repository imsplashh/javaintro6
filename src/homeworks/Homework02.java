package homeworks;

import java.util.Scanner;

public class Homework02 {
    public static void main(String[] args) {

        Scanner number = new Scanner(System.in);




        System.out.println("\n----------TASK-1----------");


        int num1, num2;

        System.out.println("The number 1 entered by user is = ");
        num1 = number.nextInt();

        System.out.println("The number 2 entered by user is = ");
        num2= number.nextInt();

        System.out.println("The sum of number 1 and 2 entered by user is = " + (num1 + num2));



        System.out.println("\n----------TASK-2----------");


        int first, second;

        System.out.println("The number 1 entered by user is = ");
        first = number.nextInt();

        System.out.println("The second number entered by user is = ");
        second = number.nextInt();

        System.out.println("The product of the given 2 numbers is: " + first * second);



        System.out.println("\n----------TASK-3----------");


        double number1, number2;

        System.out.println("The number 1 entered by user is = ");
        number1 = number.nextInt();

        System.out.println("The number 2 entered by user is = ");
        number2 = number.nextInt();

        System.out.println("The sum of the given numbers is: " + (number1 + number2));

        System.out.println("The product of the given numbers is: " + (number1 * number2));

        System.out.println("The subtraction of the given numbers is: " + (number1 - number2));

        System.out.println("The division of the given numbers is: " + (number1 / number2));

        System.out.println("The remainder of the given numbers is: " + (number1 % number2));



        System.out.println("\n----------TASK-4----------");

        System.out.println(-10 + 7 * 5);

        System.out.println((72+24) % 24);

        System.out.println(10 + -3*9/9);

        System.out.println(5 + 18/3*3-(6%3));



        System.out.println("\n----------TASK-5----------");

        int digit1, digit2;

        System.out.println("The number 1 entered by user is = ");
        digit1 = number.nextInt();

        System.out.println("The number 2 entered by user is = ");
        digit2 = number.nextInt();

        System.out.println("The average of the given numbers is: " + (digit1 +  digit2) / 2);




        System.out.println("\n----------TASK-6----------");

        int n1, n2, n3, n4, n5;

        System.out.println("The number 1 entered by user is = ");
        n1 = number.nextInt();

        System.out.println("The number 2 entered by user is = ");
        n2 = number.nextInt();

        System.out.println("The number 3 entered by user is = ");
        n3 = number.nextInt();

        System.out.println("The number 4 entered by user is = ");
        n4 = number.nextInt();

        System.out.println("The number 5 entered by user is = ");
        n5 = number.nextInt();


        System.out.println("The average of the given numbers is: " + (n1 +  n2 + n3 + n4 + n5) / 5);





        System.out.println("\n----------TASK-7----------");


        int f1, f2, f3;

        System.out.println("The number 1 entered by user is = ");
        f1 = number.nextInt();


        System.out.println("The number 2 entered by user is = ");
        f2 = number.nextInt();


        System.out.println("The number 3 entered by user is = ");
        f3 = number.nextInt();


        System.out.println("The 5 multiplied with 5 is = " + f1 * f1);
        System.out.println("The 6 multiplied with 6 is = " + f2 * f2);
        System.out.println("The 10 multiplied with 10 is = " + f3 * f3);



        System.out.println("\n----------TASK-8----------");

        int input;

        System.out.println("The side of the square is: ");
        input = number.nextInt();




        System.out.println("Perimeter of the square = " + input * 4);


        System.out.println("Area of the square = " + input * input);



        System.out.println("\n----------TASK-9----------");

        double salary = 90000.0;

        System.out.println("A Software Engineer in Test can earn $" + salary * 3);



        System.out.println("\n----------TASK-10----------");

        String favBook, favColor, favNumber;

        System.out.println("The favorite book entered by the user is = ");
        favBook = number.next();

        System.out.println("The favorite color entered by the user is = ");
        favColor = number.next();

        System.out.println("The favorite number entered by the user is = ");
        favNumber = number.next();


        System.out.println("User's favorite book is: " + favBook + "\n" + "User's favorite color is: " + favColor + "\n" + "User's favorite number is: " + favNumber);



        System.out.println("\n----------TASK-11----------");


        String firstName, lastName, age, emailAddress, phoneNumber, address;

        System.out.println("Enter your first name: ");
        firstName = number.next();

        System.out.println("Enter your last name: ");
        lastName = number.next();

        System.out.println("Enter your age: ");
        age = number.next();

        System.out.println("Enter your email address: ");
        emailAddress = number.next();

        System.out.println("Enter your phone number: ");
        phoneNumber = number.next();

        System.out.println("Enter your address: ");
        address = number.next();

        System.out.println("\t User who joined this program is " + firstName + " " + lastName+". " + "John's age is " + age+". " + "John's email \n address is " + emailAddress+", phone number is " + phoneNumber+", and address \n is " + address +".");


    }
}
