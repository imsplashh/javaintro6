package homeworks;

public class Homework17 {
    public static void main(String[] args) {

        System.out.println("======= TASK 1 ========");
        System.out.println(nthWord("I like programming languages", 2));

        System.out.println("======= TASK 2 ========");
        System.out.println(isArmStrong(153));

        System.out.println("======= TASK 3 ========");
        System.out.println(reverseNumber(371));





    }

    /**
     *  System.out.println("======= TASK 1 ========");
     * Requirement:
     * Write a method called as nthWord() that takes a String and an int arguments and returns the nth word in the String.
     *
     * NOTE: your method should return empty string if int argument is greater than the count of the words in the String.
     */

    public static String nthWord(String inputString, int n) {
        // Split the inputString into words
        String[] words = inputString.split("\\s+");

        // Check if n is within the range of the words array
        if (n > 0 && n <= words.length) {
            return words[n - 1]; // Return the nth word (subtract 1 to match array index)
        } else {
            return ""; // Return an empty string if n is out of range
        }
    }

    /**  System.out.println("======= TASK 2 ========");
     *
     * Requirement:
     * Write a method called as isArmStrong() that takes an int argument and returns true if given number is arm-strong, and false otherwise.
     * NOTE: An Armstrong number is one whose sum of digits raised to the power three equals the number itself.
     * Let's take an example to understand it better. Consider the number 153.
     * To determine if 153 is an Armstrong number, we need to check if the sum of its digits, each raised to the power of the number of digits, equals the original number.
     * 1^3 + 5^3 + 3^3 = 1 + 125 + 27 = 153
     * In this case, the sum of the individual digits raised to the power of 3 (the number of digits in 153) is equal to the original number, which means 153 is an Armstrong number.
     */

    public static boolean isArmStrong(int number) {
        int originalNumber = number;
        int sum = 0;

        while (number != 0) {
            int digit = number % 10;
            sum += digit * digit * digit;
            number /= 10;
        }

        return (sum == originalNumber);
    }

    /**  System.out.println("======= TASK 3 ========");
     *
     *   Requirement:
     * Write a method reverseNumber() that takes an int argument and returns it back reversed without converting it to a String.
     *
     * NOTE: DO NOT CONVERT NUMBER TO A STRING TO REVERSE IT!!!
     */


    public static int reverseNumber(int number) {
        int reversedNumber = 0;

        while (number != 0) {
            int digit = number % 10;
            reversedNumber = reversedNumber * 10 + digit;
            number /= 10;
        }

        return reversedNumber;
    }



}
