package regex;

import java.util.regex.Pattern;

public class PhoneNumberRegex {
    public static void main(String[] args) {

        String phoneNumberRegex = "[(]?[0-9]{3}[)]?-[0-9]{3}-[0-9]{4}";
        System.out.println(Pattern.matches(phoneNumberRegex, "(123)-123-1234"));

        // (xxx)-xxx-xxxx
        // (xxx) -> [0-9] {3}
        // xxx
        // xxxx
    }
}
