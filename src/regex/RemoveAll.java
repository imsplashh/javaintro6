package regex;

import java.util.regex.Pattern;

public class RemoveAll {
    public static void main(String[] args) {
        //count vowels (without regex)
        int count = 0;

        String str = "Hello, my name is Jehad and i love TechGlobal";

        for (char c : str.toCharArray()) {
            if(Character.toLowerCase(c) == 'a' || Character.toLowerCase(c) == 'e' || Character.toLowerCase(c) == 'i'
                    || Character.toLowerCase(c) == 'o'|| Character.toLowerCase(c) == 'u') count++;
        }
        System.out.println("The word has " + count + " vowels");

        // count vowels (regex)

        System.out.println("===== regex removeAll ====");

       str = str.replaceAll("[^aeiouAEIOU]", "");

        System.out.println("The words has " + str.length() + " vowels");







    }
}
