package regex;

import utilities.ScannerHelper;

import java.util.regex.Pattern;

public class ValidUserName {
    public static void main(String[] args) {

        /*
        Ask the user to enter a username.
        If the username matches the format of [a-zA-Z0-9]{5,10}, print out
        "Valid Username".

        If it does not match the desired format then print out "Error!
        Username must be 5 to 10 characters long and can only contain
        letters and numbers"
        Scenario 1:
        Program: Please enter a username
        User: JDoe123
        Program: Valid Username
        Scenario 2:
        Program: Please enter a username
        User: John Doe
        Program: Error! Username must be 5 to 10 characters long and
        can only contain letters and numbers
         */

        String input = ScannerHelper.getString();

        if(Pattern.matches("[a-zA-Z0-9]{5,10}", input)){
            System.out.println("Valid Username");
        } else System.out.println("Error! Username must be 5 to 10 characters long and can only contain letters and numbers");

    }
}
