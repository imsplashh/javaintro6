package regex;

import utilities.ScannerHelper;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CountWords {
    public static void main(String[] args) {


        Pattern pattern = Pattern.compile("[A-Za-z]{1,}");
        Matcher matcher = pattern.matcher("I love Java, Java is fun");

        int counter = 0;

        while(matcher.find()){
            counter++;
            System.out.println(matcher.group());
        }
        System.out.println("This sentence contains " + counter + " words");



    }
}
