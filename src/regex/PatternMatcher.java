package regex;

import utilities.ScannerHelper;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PatternMatcher {
    public static void main(String[] args) {

        String input = ScannerHelper.getString();

        if(Pattern.matches("[a-z0-9_-]{3,10}", input)){

            System.out.println("Thank you!!");
        } else System.out.println("This string must follow conditions");


        Pattern pattern = Pattern.compile("Java");
        Matcher matcher = pattern.matcher("I love Java, Java is fun");

        System.out.println(pattern);
        System.out.println(pattern.toString());
        System.out.println(pattern.pattern());

        System.out.println(matcher.matches());


        System.out.println("===== while loop ======");

        int counter = 0;

        while(matcher.find()){
            counter++;
            System.out.println(matcher.group());
            System.out.println(matcher.start());
            System.out.println(matcher.end());
        }
        System.out.println("Java count = " + counter);
    }
}
