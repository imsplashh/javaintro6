package random_class;

import java.util.Random;

public class RandomNumbers {
    public static void main(String[] args) {

        Random r = new Random();

       int num1 = r.nextInt(2) ; // numbers between 0 and 1
       int num2 = r.nextInt(2) + 10; // numbers between 10 and 11

        System.out.println(num1);
        System.out.println(num2);


    }
}
